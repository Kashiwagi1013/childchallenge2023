﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void FadeSample::FadeScene()
extern void FadeSample_FadeScene_m5040FFF3B4D11938786088E9F4868143D8F3D3F5 (void);
// 0x00000002 System.Void FadeSample::.ctor()
extern void FadeSample__ctor_m0B8AC327E21BAE813CBDD4B673FFD4002507D602 (void);
// 0x00000003 FadeManager FadeManager::get_Instance()
extern void FadeManager_get_Instance_m1E17C8CF614C3137EDE238BBD88B9DC5C832227B (void);
// 0x00000004 System.Void FadeManager::Awake()
extern void FadeManager_Awake_m14E21DEDD568CD00897E9DEFC02177930A2A77DD (void);
// 0x00000005 System.Void FadeManager::OnGUI()
extern void FadeManager_OnGUI_mA28ED27769B96A95DA246047252C7954617B3711 (void);
// 0x00000006 System.Void FadeManager::LoadScene(System.String,System.Single)
extern void FadeManager_LoadScene_mDE65F4040BA07014AEC85194A76AA504CBA0FA5A (void);
// 0x00000007 System.Collections.IEnumerator FadeManager::TransScene(System.String,System.Single)
extern void FadeManager_TransScene_m8E515DAFF5E47817D8B35431F5E853D9EDF6F9EE (void);
// 0x00000008 System.Void FadeManager::.ctor()
extern void FadeManager__ctor_m129C223D3AF5911C01E82B77FBA4266FA0CBD153 (void);
// 0x00000009 System.Void FadeManager/<TransScene>d__10::.ctor(System.Int32)
extern void U3CTransSceneU3Ed__10__ctor_m5371B8C985711A279AEFEBF1CE27CC1804511EAD (void);
// 0x0000000A System.Void FadeManager/<TransScene>d__10::System.IDisposable.Dispose()
extern void U3CTransSceneU3Ed__10_System_IDisposable_Dispose_m9F4A2B9609FB3B22FBF0DB8EF3220CF28614DC1E (void);
// 0x0000000B System.Boolean FadeManager/<TransScene>d__10::MoveNext()
extern void U3CTransSceneU3Ed__10_MoveNext_m6BB3E29EB9A1DFCD6A197211F97ACE7EBE7B15CB (void);
// 0x0000000C System.Object FadeManager/<TransScene>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71B91B76729E1BB68B7F950661891929319F5C69 (void);
// 0x0000000D System.Void FadeManager/<TransScene>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTransSceneU3Ed__10_System_Collections_IEnumerator_Reset_mA196A4E32EB839966AD0D6FA04DB8E0D29E6330D (void);
// 0x0000000E System.Object FadeManager/<TransScene>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTransSceneU3Ed__10_System_Collections_IEnumerator_get_Current_mF14EC66728AD2648CAC780E643FF20AE0E9D0914 (void);
// 0x0000000F System.Void AppleCatchManager::Start()
extern void AppleCatchManager_Start_mA7454944A5868CE61B5BD6A17C5D0BBCC2E44589 (void);
// 0x00000010 System.Void AppleCatchManager::FixedUpdate()
extern void AppleCatchManager_FixedUpdate_m1B3449F813C01C2E4068D4968479646FBA2CA509 (void);
// 0x00000011 System.Void AppleCatchManager::ItemInstanceSetParameter(System.Single,System.Int32)
extern void AppleCatchManager_ItemInstanceSetParameter_m9FEFEAE7183F4713C4ED573DCB507755C2891B20 (void);
// 0x00000012 System.Void AppleCatchManager::GameSetStart()
extern void AppleCatchManager_GameSetStart_m37C5A1C5444E858A570CB2FA57F4FAF953590DBC (void);
// 0x00000013 System.Void AppleCatchManager::ItemInstance()
extern void AppleCatchManager_ItemInstance_mEDD19EF2FC76E833A0DCE15204CF7E98369C786A (void);
// 0x00000014 System.Void AppleCatchManager::GetApple()
extern void AppleCatchManager_GetApple_mA597A63B94F1300C4E7272DC3E38A2813D0314FA (void);
// 0x00000015 System.Void AppleCatchManager::GetInsect()
extern void AppleCatchManager_GetInsect_m5C8B71705C07BEAA97EC960F232BF5DAB9AACFC5 (void);
// 0x00000016 System.Void AppleCatchManager::OnClickBackButton()
extern void AppleCatchManager_OnClickBackButton_m320AA1B9A9C72B40D1DDDF2CA3C3CA0B9B1B6AD9 (void);
// 0x00000017 System.Void AppleCatchManager::OnClickReStartButton(System.Int32)
extern void AppleCatchManager_OnClickReStartButton_m9F85E502FB4B8255F9D16E204D3E4DBB4A0F6B53 (void);
// 0x00000018 System.Void AppleCatchManager::ReStartButtonActive()
extern void AppleCatchManager_ReStartButtonActive_m482340B56FC27737AD635C5BFE0C6C1E80E2B0A0 (void);
// 0x00000019 System.Void AppleCatchManager::.ctor()
extern void AppleCatchManager__ctor_mDEFD24CA16BCF531B5E5EE3FD118DDEA36D1CBD7 (void);
// 0x0000001A System.Void AppleCatchPlayerController::Start()
extern void AppleCatchPlayerController_Start_mCD599400153614623A6DD00CC71006678F8B6E59 (void);
// 0x0000001B System.Void AppleCatchPlayerController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void AppleCatchPlayerController_OnCollisionEnter2D_mBDF7DCC95A254E7966D55C7DB296FF5F145C726A (void);
// 0x0000001C System.Void AppleCatchPlayerController::OnMouseDrag()
extern void AppleCatchPlayerController_OnMouseDrag_mE7A2F423B18848E150E093056CB0C274CAF2D5DF (void);
// 0x0000001D System.Void AppleCatchPlayerController::.ctor()
extern void AppleCatchPlayerController__ctor_m1795AEB17CC7E18C571FAA7F91BF8A6D94E96EBB (void);
// 0x0000001E System.Void AppleCatchStageSlectManager::Start()
extern void AppleCatchStageSlectManager_Start_m6888DAC4A82958EC25A3D39808610F344DBD5F68 (void);
// 0x0000001F System.Void AppleCatchStageSlectManager::OnClickSelectButton(System.Int32)
extern void AppleCatchStageSlectManager_OnClickSelectButton_m604EE1AE6FD2B8753164BCE3992BE8EFBEFA0879 (void);
// 0x00000020 System.Void AppleCatchStageSlectManager::OnClickBackButton()
extern void AppleCatchStageSlectManager_OnClickBackButton_mDA720852F329AFEFB324A413411339872FE49D28 (void);
// 0x00000021 System.Void AppleCatchStageSlectManager::.ctor()
extern void AppleCatchStageSlectManager__ctor_m54DA6BA5060DC6E3E58701F452E9BA60504F5AD2 (void);
// 0x00000022 System.Void ItemPrefabController::FixedUpdate()
extern void ItemPrefabController_FixedUpdate_m249A9BA46B5D5D194AD4671EFE5941387416B907 (void);
// 0x00000023 System.Void ItemPrefabController::ItemPrefabControll()
extern void ItemPrefabController_ItemPrefabControll_mA80B74C8A248DF3E9FA82F06A28D42F07A5C678D (void);
// 0x00000024 System.Void ItemPrefabController::.ctor()
extern void ItemPrefabController__ctor_m2481493864C55C12ECC59DF16CA80807C9EF188F (void);
// 0x00000025 System.Void CameraStableAspect::Awake()
extern void CameraStableAspect_Awake_m8BB1F059EB334778D8241BF71B81865FA18C9139 (void);
// 0x00000026 System.Void CameraStableAspect::Update()
extern void CameraStableAspect_Update_m16BA8EAA8AAF545332B9468A1A8D56182394510C (void);
// 0x00000027 System.Void CameraStableAspect::UpdateCameraWithCheck()
extern void CameraStableAspect_UpdateCameraWithCheck_m33767755B9E5FCA6B25261067A97868E2265A33F (void);
// 0x00000028 System.Void CameraStableAspect::UpdateCamera()
extern void CameraStableAspect_UpdateCamera_mD088EA93E094AF45BAB9D7E790164A3F470B1556 (void);
// 0x00000029 System.Void CameraStableAspect::.ctor()
extern void CameraStableAspect__ctor_mB3FBCB60546A52BB2B2297003BD3E273EDBF29C0 (void);
// 0x0000002A System.Void RectScalerWithViewport::Awake()
extern void RectScalerWithViewport_Awake_mFA199E5E1D6C1EA5F7EC6270FAF78A39B96AE011 (void);
// 0x0000002B System.Void RectScalerWithViewport::Update()
extern void RectScalerWithViewport_Update_m76273C7F0EF5FFD8ADCA193D252E938801F6AB24 (void);
// 0x0000002C System.Void RectScalerWithViewport::OnValidate()
extern void RectScalerWithViewport_OnValidate_mE45284F8A4AFDF2C6E7055F5EE91403F8D3E5B4C (void);
// 0x0000002D System.Void RectScalerWithViewport::UpdateRectWithCheck()
extern void RectScalerWithViewport_UpdateRectWithCheck_m4CC8EF1FFB35DB6D1A174E58971A45CE406F7DAB (void);
// 0x0000002E System.Void RectScalerWithViewport::UpdateRect()
extern void RectScalerWithViewport_UpdateRect_m4FA8D4B76027E1EBB04493D8B5EE60A2F88538A9 (void);
// 0x0000002F System.Void RectScalerWithViewport::.ctor()
extern void RectScalerWithViewport__ctor_m61613DD9F4F9422F089F94EA7ED44084985499BA (void);
// 0x00000030 System.Void ColorMatchingStageSlectManager::Start()
extern void ColorMatchingStageSlectManager_Start_m349A961200EDD46EDC7337ACA02790620B856861 (void);
// 0x00000031 System.Void ColorMatchingStageSlectManager::Update()
extern void ColorMatchingStageSlectManager_Update_mE8EB93B613543276CD60AE2C504BCBAB59908002 (void);
// 0x00000032 System.Void ColorMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ColorMatchingStageSlectManager_PushStageSelectButton_m2BBDBA62C6448BD5434F4F58F3FA6E6474A10A6B (void);
// 0x00000033 System.Void ColorMatchingStageSlectManager::BackButton()
extern void ColorMatchingStageSlectManager_BackButton_mB83E54677D069B5E69377FDE057744436649AEA0 (void);
// 0x00000034 System.Void ColorMatchingStageSlectManager::GobackStageSelect()
extern void ColorMatchingStageSlectManager_GobackStageSelect_m028422CADC90DBF5FFF829C4E9868EF5B873D739 (void);
// 0x00000035 System.Void ColorMatchingStageSlectManager::.ctor()
extern void ColorMatchingStageSlectManager__ctor_mBEF5EC4D6223D31932DA6EFDE786717703182798 (void);
// 0x00000036 System.Void CourseSelectManager::Start()
extern void CourseSelectManager_Start_mCFB41539F608FFD4C26446E3F53262E6D92D6A8A (void);
// 0x00000037 System.Void CourseSelectManager::OnClickStageSelectButton(System.Int32)
extern void CourseSelectManager_OnClickStageSelectButton_m954D82A504F80B1A48C05D264C410980B08A087A (void);
// 0x00000038 System.Void CourseSelectManager::OnClickBackButton()
extern void CourseSelectManager_OnClickBackButton_m1706C1AC1AE656A1284FFF64088887F9518699A6 (void);
// 0x00000039 System.Void CourseSelectManager::.ctor()
extern void CourseSelectManager__ctor_m5F1ED0AA2079CA6C3AF431F86BF389D85A383960 (void);
// 0x0000003A System.Void GoForTheGoalStageSlectManager::Start()
extern void GoForTheGoalStageSlectManager_Start_mA559068A80277788B67E5BB7201BA51A986DAEB8 (void);
// 0x0000003B System.Void GoForTheGoalStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void GoForTheGoalStageSlectManager_OnClickStageSelectButton_m0A00CE28E9D028F5AF98DA86431CAD73F2180E2F (void);
// 0x0000003C System.Void GoForTheGoalStageSlectManager::OnClickBackButton()
extern void GoForTheGoalStageSlectManager_OnClickBackButton_m92483CAD7EEF032C70352A04A89396C772FBCEDC (void);
// 0x0000003D System.Void GoForTheGoalStageSlectManager::.ctor()
extern void GoForTheGoalStageSlectManager__ctor_m1A228B973AAB16BBDBF53E1A62C14F7441A757BB (void);
// 0x0000003E System.Void PlayerController::Start()
extern void PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF (void);
// 0x0000003F System.Void PlayerController::Update()
extern void PlayerController_Update_m1F4051EB5BCBCCE5EEE2E3E49B7E278C3B14EC33 (void);
// 0x00000040 System.Void PlayerController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerController_OnCollisionEnter2D_m28C67E4361403BA9990C1E6D9526F78362591667 (void);
// 0x00000041 System.Void PlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerController_OnTriggerEnter2D_mB82BB8D400DC3E0E6B7117AE1A94E72E99CF53FB (void);
// 0x00000042 System.Void PlayerController::PlayerSpeedPower()
extern void PlayerController_PlayerSpeedPower_mC2D8CA83587680EA80269954CB1D59A93A3F71F1 (void);
// 0x00000043 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33 (void);
// 0x00000044 System.Void MoveBaseManager::OnMouseDrag()
extern void MoveBaseManager_OnMouseDrag_mE8378022FC2F0C6DDA8FB362CABF22DA70080775 (void);
// 0x00000045 System.Void MoveBaseManager::.ctor()
extern void MoveBaseManager__ctor_mD1F65D2189C2448890BB8CE47C5261AF5DE41E9B (void);
// 0x00000046 System.Void PalmAndGoalGameManager::Start()
extern void PalmAndGoalGameManager_Start_m93D80E951B34EDB1D23FC4804AEFA865E9370100 (void);
// 0x00000047 System.Void PalmAndGoalGameManager::OnClickGoButton()
extern void PalmAndGoalGameManager_OnClickGoButton_m95360ED9A3BB4D09A25B33570492486F37088ED3 (void);
// 0x00000048 System.Void PalmAndGoalGameManager::OnClickRetryButton()
extern void PalmAndGoalGameManager_OnClickRetryButton_m21A41CABF66A119EF1F8B7660098FD68A9EDBCDE (void);
// 0x00000049 System.Void PalmAndGoalGameManager::OnClickGoalBackButton()
extern void PalmAndGoalGameManager_OnClickGoalBackButton_m2B76FFA3D60982253E0ED7B5521225EE63E5174C (void);
// 0x0000004A System.Void PalmAndGoalGameManager::OnClickPalmBackButton()
extern void PalmAndGoalGameManager_OnClickPalmBackButton_mE5D41ECEF572861480AFCD6D7C66C7D5484DEEEA (void);
// 0x0000004B System.Void PalmAndGoalGameManager::StageClear()
extern void PalmAndGoalGameManager_StageClear_m314CAFCF5362DAFC6AD952948FEF5E2ABD7D276D (void);
// 0x0000004C System.Void PalmAndGoalGameManager::.ctor()
extern void PalmAndGoalGameManager__ctor_mA9C4F2F968785B32406D61BF1F3AC8C862EE550C (void);
// 0x0000004D System.Void PalmFruitCatchStageSlectManager::Start()
extern void PalmFruitCatchStageSlectManager_Start_m3D4F4F67820CA1AA0295ECDA86C39AE1DCAEB686 (void);
// 0x0000004E System.Void PalmFruitCatchStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void PalmFruitCatchStageSlectManager_OnClickStageSelectButton_mAD40F2FD27B9E30A1515EB80E6304E2F609AA14A (void);
// 0x0000004F System.Void PalmFruitCatchStageSlectManager::OnClickBackButton()
extern void PalmFruitCatchStageSlectManager_OnClickBackButton_m2E193CA26F5199C718E7122B54FD5E9975703A18 (void);
// 0x00000050 System.Void PalmFruitCatchStageSlectManager::.ctor()
extern void PalmFruitCatchStageSlectManager__ctor_m9F8A4396354BED45BDC7A5B36F7027394177BA3C (void);
// 0x00000051 System.Void PalmFruitlManager::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PalmFruitlManager_OnCollisionEnter2D_mF063608C7F3BC77F82DE57EDD1AC417956E4C7EA (void);
// 0x00000052 System.Void PalmFruitlManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PalmFruitlManager_OnTriggerEnter2D_mC009BA8948279EF6CF5B650249D4D0D9F08C20D5 (void);
// 0x00000053 System.Void PalmFruitlManager::.ctor()
extern void PalmFruitlManager__ctor_m939CE278E957EF1BA8E40621C3B1F985E3FBD004 (void);
// 0x00000054 System.Void MatchingManager::Start()
extern void MatchingManager_Start_m88ED67277DA452A820943869A60379D61E310F74 (void);
// 0x00000055 System.Void MatchingManager::Update()
extern void MatchingManager_Update_m4CD0E2179BFE9271568B603303CADA1DD48B042D (void);
// 0x00000056 System.Void MatchingManager::OnClickShapeBackButton()
extern void MatchingManager_OnClickShapeBackButton_mE64C4F8754DC04631AF2B7652540154F492314E1 (void);
// 0x00000057 System.Void MatchingManager::OnClickSoundBackButton()
extern void MatchingManager_OnClickSoundBackButton_m5CB7F7785DDC1B7A9E3AA5F471EE7C81B583A86A (void);
// 0x00000058 System.Void MatchingManager::OnClickColorBackButton()
extern void MatchingManager_OnClickColorBackButton_mA4613602FD110386371C90F7C823870548ABB08A (void);
// 0x00000059 System.Void MatchingManager::OnClickThingsBackButton()
extern void MatchingManager_OnClickThingsBackButton_m05F2D7D19FA4DFD69D4C1B66413069EBDB1F7183 (void);
// 0x0000005A System.Void MatchingManager::SuccessShape1()
extern void MatchingManager_SuccessShape1_mE39D26BA6C29599E8FD2DFE3CAE80E0749123EE1 (void);
// 0x0000005B System.Void MatchingManager::SuccessShape2()
extern void MatchingManager_SuccessShape2_m59DD23C7D8414EDD4AE843B2849F00437D8562B0 (void);
// 0x0000005C System.Void MatchingManager::SuccessShape3()
extern void MatchingManager_SuccessShape3_m65594F4D1618A49EF9EE9DDCD4040034F68EB2F3 (void);
// 0x0000005D System.Void MatchingManager::SuccessShape4()
extern void MatchingManager_SuccessShape4_mE476FA557FFC7ED0E928AC0ABB118DBE61EB26D2 (void);
// 0x0000005E System.Void MatchingManager::AllSuccesGameClear()
extern void MatchingManager_AllSuccesGameClear_m1513CA9D97BEB8293C1A4B3D33BAEE9F0893F74E (void);
// 0x0000005F System.Void MatchingManager::OnClickSoundButton1()
extern void MatchingManager_OnClickSoundButton1_m494BB4B7E18027AD7C6C1EA166D44C1F1ECE10D8 (void);
// 0x00000060 System.Void MatchingManager::OnClickSoundButton2()
extern void MatchingManager_OnClickSoundButton2_m595BA36E1B8EF05D5FA1F5CE2629F1E17E951D40 (void);
// 0x00000061 System.Void MatchingManager::OnClickSoundButton3()
extern void MatchingManager_OnClickSoundButton3_mAEC35AA6461917ABDA97B0060C1D297F4F55652D (void);
// 0x00000062 System.Void MatchingManager::OnClickSoundButton4()
extern void MatchingManager_OnClickSoundButton4_m912585168E1835344F4DA7AD973A735FFA61FD30 (void);
// 0x00000063 System.Void MatchingManager::.ctor()
extern void MatchingManager__ctor_mF042FF850EA53DFAFE25508BC9CA297D2F7C1D42 (void);
// 0x00000064 System.Void ShapeController1::Start()
extern void ShapeController1_Start_mDD5CC39FB3D459FBDEB472BAA9D8CEDF8B700872 (void);
// 0x00000065 System.Void ShapeController1::Update()
extern void ShapeController1_Update_m5C2AC821891A25F948A9F6E9807E5B4A9D5F3D14 (void);
// 0x00000066 System.Void ShapeController1::ShapeSuccesClear()
extern void ShapeController1_ShapeSuccesClear_m7968F143468C05209655AE675F6E70ED66C63DAD (void);
// 0x00000067 System.Void ShapeController1::ShapeParticle()
extern void ShapeController1_ShapeParticle_mBA4E5F47494B5848F7FDED83BE3EE9B89404A7D1 (void);
// 0x00000068 System.Void ShapeController1::OnMouseDrag()
extern void ShapeController1_OnMouseDrag_m95B73E06BD4D358E884AE4A5053C0EBD11D8D595 (void);
// 0x00000069 System.Void ShapeController1::.ctor()
extern void ShapeController1__ctor_mDDFE884B945177EBEAE05DEB9418209A7241AB56 (void);
// 0x0000006A System.Void ShapeController2::Start()
extern void ShapeController2_Start_m8A79AF5212F608435E6ABE253418811B2F7A2FAC (void);
// 0x0000006B System.Void ShapeController2::Update()
extern void ShapeController2_Update_mF2E7C6E884E4E9C77B0DC11F8CEDB23BB1C0AA42 (void);
// 0x0000006C System.Void ShapeController2::ShapeSuccesClear()
extern void ShapeController2_ShapeSuccesClear_m94469D9F43BC87043828EE3D79BC5E9B7E2A47E8 (void);
// 0x0000006D System.Void ShapeController2::ShapeParticle()
extern void ShapeController2_ShapeParticle_mFB50597B7DB5517A92C1423A2C5B8042F80B629B (void);
// 0x0000006E System.Void ShapeController2::OnMouseDrag()
extern void ShapeController2_OnMouseDrag_mCBDA60AF2F1A44DE14DC85BF953AF864B1A12162 (void);
// 0x0000006F System.Void ShapeController2::.ctor()
extern void ShapeController2__ctor_mCA53A78BA95EAE8699DD6F19387CE292E768A810 (void);
// 0x00000070 System.Void ShapeController3::Start()
extern void ShapeController3_Start_m19528F95B9E01CAC470E1C62C32C25A3883D36D0 (void);
// 0x00000071 System.Void ShapeController3::Update()
extern void ShapeController3_Update_m0436D2609766AE2C582144C5DFF413E3D78E9551 (void);
// 0x00000072 System.Void ShapeController3::ShapeSuccesClear()
extern void ShapeController3_ShapeSuccesClear_mC1736059C9E735A467B8BBEA0B0C2D3ADAE77A02 (void);
// 0x00000073 System.Void ShapeController3::ShapeParticle()
extern void ShapeController3_ShapeParticle_mBFEA5B7E1CFC6BD6F1AABD7A9962AC8F3B2FA576 (void);
// 0x00000074 System.Void ShapeController3::OnMouseDrag()
extern void ShapeController3_OnMouseDrag_mE282E7BB1731816EAA78E4D4F7219DED0C2F9ED0 (void);
// 0x00000075 System.Void ShapeController3::.ctor()
extern void ShapeController3__ctor_m3C4FE6ED11431F36223AA09682E37526E30D9B6A (void);
// 0x00000076 System.Void ShapeController4::Start()
extern void ShapeController4_Start_m9483E323A13D9A302430732A19A88B91DE10C928 (void);
// 0x00000077 System.Void ShapeController4::Update()
extern void ShapeController4_Update_mA8768F6C2E5A927F59F6147384C45C17E0652090 (void);
// 0x00000078 System.Void ShapeController4::ShapeSuccesClear()
extern void ShapeController4_ShapeSuccesClear_m741C9BAC7B0D08E3F81B369654C138991558BD63 (void);
// 0x00000079 System.Void ShapeController4::ShapeParticle()
extern void ShapeController4_ShapeParticle_m946EE310F989E95159EAAD1906B07F2FEF2EF88A (void);
// 0x0000007A System.Void ShapeController4::OnMouseDrag()
extern void ShapeController4_OnMouseDrag_mF1C79A50AC442DC5D6BCED4806DDCF6AC19F02FE (void);
// 0x0000007B System.Void ShapeController4::.ctor()
extern void ShapeController4__ctor_m6FE848AE78127721F848D4666CDA8E20F059CD14 (void);
// 0x0000007C System.Boolean CardController::get_isSelected()
extern void CardController_get_isSelected_m925DFAAD2D2EB0328859E1F44DE4781A58054C6F (void);
// 0x0000007D System.Void CardController::Set(CardData)
extern void CardController_Set_mCF3346623A8E669C129630E3CF28D28EA36B2127 (void);
// 0x0000007E System.Void CardController::OnClick()
extern void CardController_OnClick_mD92C25AAA7E20330387721CDDAB4E00284B9E057 (void);
// 0x0000007F System.Void CardController::onRotate(System.Action)
extern void CardController_onRotate_m41A23FF3DBCB6CB642F622DC9AB6AA289595931F (void);
// 0x00000080 System.Void CardController::onReturnRotate(System.Action)
extern void CardController_onReturnRotate_m9C5BA2C62CE87EE4ECC0D35C9F5E6AAB3000D386 (void);
// 0x00000081 System.Void CardController::SetHide()
extern void CardController_SetHide_mBC2818BE5851FA2255B7EED3795CFA26C0D59B03 (void);
// 0x00000082 System.Void CardController::SetInvisible()
extern void CardController_SetInvisible_m2ABEEDDD0FBB92D7B38CC836ED47DBA2A5BC741C (void);
// 0x00000083 System.Void CardController::.ctor()
extern void CardController__ctor_m3593D3890C1A596631E325FB2C9C07D68364980D (void);
// 0x00000084 System.Void CardController::<OnClick>b__9_0()
extern void CardController_U3COnClickU3Eb__9_0_mF225D7032A108B302B15A8E373D6AB15959B1F67 (void);
// 0x00000085 System.Void CardController::<OnClick>b__9_1()
extern void CardController_U3COnClickU3Eb__9_1_m383EFE5F7A686E3FE4B5BDB288F89FDCBE1F4577 (void);
// 0x00000086 System.Void CardController::<SetHide>b__12_0()
extern void CardController_U3CSetHideU3Eb__12_0_m06136A22B90E8092BB5F8DFC9EC9D4C19C3BE29B (void);
// 0x00000087 System.Void CardController/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m949970D641C88B9D0B54FA2F0513BC91F9E496E8 (void);
// 0x00000088 System.Void CardController/<>c__DisplayClass10_0::<onRotate>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3ConRotateU3Eb__0_m28206B02A681E27CC17085697A0CA1BB06A16B0A (void);
// 0x00000089 System.Void CardController/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m0AF819C3403D8A5EA4352DD9AC2AE4D970FF8964 (void);
// 0x0000008A System.Void CardController/<>c__DisplayClass11_0::<onReturnRotate>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3ConReturnRotateU3Eb__0_m58891DB77BE41BCBE3586C753A8A1A2223167225 (void);
// 0x0000008B System.Void CardController/<>c::.cctor()
extern void U3CU3Ec__cctor_m4AFCC61DC6DC0879C8695AC71B2C2AAFD182A60F (void);
// 0x0000008C System.Void CardController/<>c::.ctor()
extern void U3CU3Ec__ctor_m10BACD9520D150E638331AB180D71D221E5D4D39 (void);
// 0x0000008D System.Void CardController/<>c::<SetHide>b__12_1()
extern void U3CU3Ec_U3CSetHideU3Eb__12_1_m12DB67AE49512D74BF7B3FC50DBA43E477E89239 (void);
// 0x0000008E System.Int32 CardData::get_Id()
extern void CardData_get_Id_m2F2F67A6B6E18F66DD2884A47404A5AA6DF47742 (void);
// 0x0000008F System.Void CardData::set_Id(System.Int32)
extern void CardData_set_Id_mB775FDEC373EFC4D8B45C369F09E9A6675EAAB40 (void);
// 0x00000090 UnityEngine.Sprite CardData::get_ImgSprite()
extern void CardData_get_ImgSprite_m29E9FB86757F8F381F72665D70055737F78A41EA (void);
// 0x00000091 System.Void CardData::set_ImgSprite(UnityEngine.Sprite)
extern void CardData_set_ImgSprite_mD7E883ED6BE6E724E96F76C33002EDD330F655CD (void);
// 0x00000092 System.Void CardData::.ctor(System.Int32,UnityEngine.Sprite)
extern void CardData__ctor_mAB5EBF5189FB88C264087C89B198DF5DF8C0F095 (void);
// 0x00000093 System.Void CardCreateManager::Start()
extern void CardCreateManager_Start_m6414124036CC51428862AA4FE25D72622203C220 (void);
// 0x00000094 System.Void CardCreateManager::GameContentsNumber()
extern void CardCreateManager_GameContentsNumber_m878F478C787B80F9658F532CAE7F04E18FA1A685 (void);
// 0x00000095 System.Void CardCreateManager::RandumCardPlacement()
extern void CardCreateManager_RandumCardPlacement_m83C2612095918D79E1F76050FDB7C3997B0063A6 (void);
// 0x00000096 System.Void CardCreateManager::CreateCard()
extern void CardCreateManager_CreateCard_mFD8D52AC87FC95F0536C57D8666885FA850F789D (void);
// 0x00000097 System.Void CardCreateManager::HideCardList(System.Collections.Generic.List`1<System.Int32>)
extern void CardCreateManager_HideCardList_mCDC58856D24B2B7FB871C45EC45FB7B39504A26B (void);
// 0x00000098 System.Void CardCreateManager::mSetDealCardAnime()
extern void CardCreateManager_mSetDealCardAnime_mD75460D7D0EE5E72FF049A48D5036462C73D9CFF (void);
// 0x00000099 System.Void CardCreateManager::.ctor()
extern void CardCreateManager__ctor_m10D96A568093E863926FF95F6A97EA03F5142B7B (void);
// 0x0000009A System.Void CardCreateManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m7198AA25F46049460F7FE7F0183C75D3B90C8630 (void);
// 0x0000009B System.Void CardCreateManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m14E82AE95EBD7E6EE5D44A1D23B58620B987DEDB (void);
// 0x0000009C System.Guid CardCreateManager/<>c::<RandumCardPlacement>b__12_0(CardData)
extern void U3CU3Ec_U3CRandumCardPlacementU3Eb__12_0_mB8351D0F9DD9389312DD41FE0DAEC00208B4FFE9 (void);
// 0x0000009D System.Guid CardCreateManager/<>c::<RandumCardPlacement>b__12_1(CardData)
extern void U3CU3Ec_U3CRandumCardPlacementU3Eb__12_1_m6D46EC79F65155C21B80437A0946B3789333C64E (void);
// 0x0000009E System.Guid CardCreateManager/<>c::<CreateCard>b__13_0(CardData)
extern void U3CU3Ec_U3CCreateCardU3Eb__13_0_m9976E972D56AAEDF0DD8F2DBC42E12650E24A2AD (void);
// 0x0000009F System.Void CardCreateManager/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m50CB1CB4BBF4EA21159C6D7C0A025608599A061D (void);
// 0x000000A0 System.Void CardCreateManager/<>c__DisplayClass15_0::<mSetDealCardAnime>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CmSetDealCardAnimeU3Eb__0_m0327DBE8CF7E23DEB9D66E5FD48DAB3438536720 (void);
// 0x000000A1 System.Void GameSceneManager::Awake()
extern void GameSceneManager_Awake_m98F814B7C0D760AEC43A4BA6B71439B010E885CD (void);
// 0x000000A2 System.Void GameSceneManager::Update()
extern void GameSceneManager_Update_mF84FC3FDB1756D98B7BBEFC5B5023F2E376F07F3 (void);
// 0x000000A3 System.Void GameSceneManager::GameContentsNumber()
extern void GameSceneManager_GameContentsNumber_m4A424968A5B4A92C86BA1200409F77AB4C5B5888 (void);
// 0x000000A4 System.Void GameSceneManager::GameContents()
extern void GameSceneManager_GameContents_mD82863EB0954603BCDF8EAD44476E2B8B452EE00 (void);
// 0x000000A5 System.Void GameSceneManager::OnClickBackButton()
extern void GameSceneManager_OnClickBackButton_m03341802C465BF3E0399A7BD80C47284CE6E90C4 (void);
// 0x000000A6 System.Void GameSceneManager::.ctor()
extern void GameSceneManager__ctor_m8BDD7367124B10E28EAB106035B280F1C6CE979E (void);
// 0x000000A7 GameStateController GameStateController::get_Instance()
extern void GameStateController_get_Instance_mB97B49A7497739799E2DF04DF9D3DB0745D3B047 (void);
// 0x000000A8 System.Void GameStateController::.ctor()
extern void GameStateController__ctor_m31EB272531625F3334844A6A0213D29ABE98DA18 (void);
// 0x000000A9 System.Void MemoryWeaknessStageSlectManager::Start()
extern void MemoryWeaknessStageSlectManager_Start_mFC5A3CF9D47F43A8F6B40D197FD72ECA9F5C967E (void);
// 0x000000AA System.Void MemoryWeaknessStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void MemoryWeaknessStageSlectManager_OnClickStageSelectButton_mC5C587F0ADAA65B27BEEA257702AC5966A8DBF28 (void);
// 0x000000AB System.Void MemoryWeaknessStageSlectManager::OnClickBackButton()
extern void MemoryWeaknessStageSlectManager_OnClickBackButton_mC9EF587E373C410C9EDB2F37FAD220251A165912 (void);
// 0x000000AC System.Void MemoryWeaknessStageSlectManager::.ctor()
extern void MemoryWeaknessStageSlectManager__ctor_m27921BA8D6BFB3E618915B8103CF1A109A001290 (void);
// 0x000000AD System.Void PrecautionaryStatementSceneChange::Start()
extern void PrecautionaryStatementSceneChange_Start_m44D212320F0A63AC8FFE8B4634568E4E8C300FF1 (void);
// 0x000000AE System.Void PrecautionaryStatementSceneChange::TitleSceneChange()
extern void PrecautionaryStatementSceneChange_TitleSceneChange_mB7DD17D17BD1D0F208DF68AB5F74550FFA8317A9 (void);
// 0x000000AF System.Void PrecautionaryStatementSceneChange::.ctor()
extern void PrecautionaryStatementSceneChange__ctor_m0960C353E75697994DFB76C3F824A91AA8E185D0 (void);
// 0x000000B0 System.Void ShapeMatchingStageSlectManager::Start()
extern void ShapeMatchingStageSlectManager_Start_m53304B2A2B71CBA48E1AA910D65D3CCD4D22CAF4 (void);
// 0x000000B1 System.Void ShapeMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ShapeMatchingStageSlectManager_PushStageSelectButton_mF32F6FA61E3186F251D23771B628A9655B1313BA (void);
// 0x000000B2 System.Void ShapeMatchingStageSlectManager::BackButton()
extern void ShapeMatchingStageSlectManager_BackButton_m12073D6F2D71B2E40A035F35B81F50E9F46682DE (void);
// 0x000000B3 System.Void ShapeMatchingStageSlectManager::GobackStageSelect()
extern void ShapeMatchingStageSlectManager_GobackStageSelect_m018AB02974C242E871E4343122B7D54E0937B5A5 (void);
// 0x000000B4 System.Void ShapeMatchingStageSlectManager::.ctor()
extern void ShapeMatchingStageSlectManager__ctor_m728CC91A62081ACE6E8A93397B12E40E30FC9AD8 (void);
// 0x000000B5 System.Void SoundMatchingStageSlectManager::Start()
extern void SoundMatchingStageSlectManager_Start_m36BBF19E4C9EDD28FCE70E628087003033BA3925 (void);
// 0x000000B6 System.Void SoundMatchingStageSlectManager::Update()
extern void SoundMatchingStageSlectManager_Update_m308042517A8C6FD15B62A39CD4C5F5D510EF12E8 (void);
// 0x000000B7 System.Void SoundMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void SoundMatchingStageSlectManager_PushStageSelectButton_m87C8D135699D2D86B578A4DCEDC3A64DE3797C73 (void);
// 0x000000B8 System.Void SoundMatchingStageSlectManager::BackButton()
extern void SoundMatchingStageSlectManager_BackButton_m8C3264F63977A8AB98C1B21765AB436117C70974 (void);
// 0x000000B9 System.Void SoundMatchingStageSlectManager::GobackStageSelect()
extern void SoundMatchingStageSlectManager_GobackStageSelect_mCBF75B86811F3A0FE56D9838FB1D4F1AC32DFF2B (void);
// 0x000000BA System.Void SoundMatchingStageSlectManager::.ctor()
extern void SoundMatchingStageSlectManager__ctor_mAA823E24E1D807F75E28EF1906B30E84919122A1 (void);
// 0x000000BB System.Void ThingsMatchingStageSlectManager::Start()
extern void ThingsMatchingStageSlectManager_Start_mFC83FB0B4365222BE74C0C220548EDBCCB737B96 (void);
// 0x000000BC System.Void ThingsMatchingStageSlectManager::Update()
extern void ThingsMatchingStageSlectManager_Update_mADAB051E88DB1058C9F1EB665D81524326D2D894 (void);
// 0x000000BD System.Void ThingsMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ThingsMatchingStageSlectManager_PushStageSelectButton_m989D61D574C3F8BCD35F64036F88C24CE8E9497B (void);
// 0x000000BE System.Void ThingsMatchingStageSlectManager::BackButton()
extern void ThingsMatchingStageSlectManager_BackButton_m65C2D2634BD5CB59C18E9A259CD6F6DCB47ED0B6 (void);
// 0x000000BF System.Void ThingsMatchingStageSlectManager::GobackStageSelect()
extern void ThingsMatchingStageSlectManager_GobackStageSelect_m110F6EAF5F42BABFE6B16924FB0D7EC0B72AA5FE (void);
// 0x000000C0 System.Void ThingsMatchingStageSlectManager::.ctor()
extern void ThingsMatchingStageSlectManager__ctor_mB31EFAC1A023B0B33B1D8FA0753B3C706EBE45DE (void);
// 0x000000C1 System.Void ExitConfirmationManager::Start()
extern void ExitConfirmationManager_Start_mA7E1E6E4BB2FED39AADEA36BED338C588038A1F1 (void);
// 0x000000C2 System.Void ExitConfirmationManager::Update()
extern void ExitConfirmationManager_Update_mAAA2A9AAE8FEE7F74F93019944C661F5279EC474 (void);
// 0x000000C3 System.Void ExitConfirmationManager::SuccessShape1()
extern void ExitConfirmationManager_SuccessShape1_m91312147112F0F3F67E41C2B41DEB3CB78285615 (void);
// 0x000000C4 System.Void ExitConfirmationManager::SuccessShape2()
extern void ExitConfirmationManager_SuccessShape2_m4A06AAFF75B56549A88E1698F12C7648C13B7D30 (void);
// 0x000000C5 System.Void ExitConfirmationManager::SuccessShape3()
extern void ExitConfirmationManager_SuccessShape3_m4F7F23E0D7406589960654C875D62035AB1BF5A2 (void);
// 0x000000C6 System.Void ExitConfirmationManager::AllSuccesExitView()
extern void ExitConfirmationManager_AllSuccesExitView_m8B6348A1D541E0FDAD953D09AFC0D7A32E74A606 (void);
// 0x000000C7 System.Void ExitConfirmationManager::.ctor()
extern void ExitConfirmationManager__ctor_m4C1187A9F2DC3B96E1AEA40BAF937281DB23788C (void);
// 0x000000C8 System.Void MainSoundManager::Awake()
extern void MainSoundManager_Awake_mCEC847FF5E831E30543705B07B35AED81D6C969B (void);
// 0x000000C9 System.Void MainSoundManager::Start()
extern void MainSoundManager_Start_m22F9FE4B2F0E499F3772C63B200BAD8A702E2315 (void);
// 0x000000CA System.Void MainSoundManager::SoundNumberSet()
extern void MainSoundManager_SoundNumberSet_m89D86506005562DCCCF09F6031086D9E851613DD (void);
// 0x000000CB System.Void MainSoundManager::OnActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void MainSoundManager_OnActiveSceneChanged_mA63D4801527858B0C3D11781B04EFF77E7EB8C80 (void);
// 0x000000CC System.Void MainSoundManager::.ctor()
extern void MainSoundManager__ctor_mF02C439B9FB0BC61A6111EF7C7B2FCFDF73213A0 (void);
// 0x000000CD System.Void TextTitle::FixedUpdate()
extern void TextTitle_FixedUpdate_mF917F50586A2FE97FE7F773F0DDD737521D408DE (void);
// 0x000000CE System.Void TextTitle::SetTextColorChange(System.Boolean,UnityEngine.UI.Text)
extern void TextTitle_SetTextColorChange_mE8006EF7517CA9C064258ECD7B157735F3D91BD4 (void);
// 0x000000CF System.Void TextTitle::.ctor()
extern void TextTitle__ctor_mA49159D4FF806BCF45C0DCACAD905C171F041714 (void);
// 0x000000D0 System.Void TextTitle::.cctor()
extern void TextTitle__cctor_m14037CE87C9B0E55B70F20C3BB50D7746A550DFC (void);
// 0x000000D1 System.Void TitleManager::Start()
extern void TitleManager_Start_m5EB0BAEFA202C6B62649F77BDE8213F8FE56F282 (void);
// 0x000000D2 System.Void TitleManager::OnClickStartButton()
extern void TitleManager_OnClickStartButton_mA217718CF58C65F0BB2DEB6ACCC376D536CC3475 (void);
// 0x000000D3 System.Void TitleManager::OnClickCreditViewButton()
extern void TitleManager_OnClickCreditViewButton_m95F943C1066DB6F26C2D62A90A352DC66A5C77C6 (void);
// 0x000000D4 System.Void TitleManager::OnClickCreditViewBackButton()
extern void TitleManager_OnClickCreditViewBackButton_m2337ACCA9C77D903ABC4FA9565F5F134921C7B69 (void);
// 0x000000D5 System.Void TitleManager::OnClickExitViewButton()
extern void TitleManager_OnClickExitViewButton_m7C891B3DADB202D8B2C7D803C853C60A4CB023ED (void);
// 0x000000D6 System.Void TitleManager::OnClickExitButton()
extern void TitleManager_OnClickExitButton_m94EABA61A584F75C223D21BB5F6F19CCD0C0CBCF (void);
// 0x000000D7 System.Void TitleManager::OnClickonfirmationViewBackButton()
extern void TitleManager_OnClickonfirmationViewBackButton_mFAF43718343DF3E02B7E2AA0EE137F370B32AB44 (void);
// 0x000000D8 System.Void TitleManager::.ctor()
extern void TitleManager__ctor_m08406351E7FEB77F80D0A3A7F3FCC58D5CAAE237 (void);
static Il2CppMethodPointer s_methodPointers[216] = 
{
	FadeSample_FadeScene_m5040FFF3B4D11938786088E9F4868143D8F3D3F5,
	FadeSample__ctor_m0B8AC327E21BAE813CBDD4B673FFD4002507D602,
	FadeManager_get_Instance_m1E17C8CF614C3137EDE238BBD88B9DC5C832227B,
	FadeManager_Awake_m14E21DEDD568CD00897E9DEFC02177930A2A77DD,
	FadeManager_OnGUI_mA28ED27769B96A95DA246047252C7954617B3711,
	FadeManager_LoadScene_mDE65F4040BA07014AEC85194A76AA504CBA0FA5A,
	FadeManager_TransScene_m8E515DAFF5E47817D8B35431F5E853D9EDF6F9EE,
	FadeManager__ctor_m129C223D3AF5911C01E82B77FBA4266FA0CBD153,
	U3CTransSceneU3Ed__10__ctor_m5371B8C985711A279AEFEBF1CE27CC1804511EAD,
	U3CTransSceneU3Ed__10_System_IDisposable_Dispose_m9F4A2B9609FB3B22FBF0DB8EF3220CF28614DC1E,
	U3CTransSceneU3Ed__10_MoveNext_m6BB3E29EB9A1DFCD6A197211F97ACE7EBE7B15CB,
	U3CTransSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71B91B76729E1BB68B7F950661891929319F5C69,
	U3CTransSceneU3Ed__10_System_Collections_IEnumerator_Reset_mA196A4E32EB839966AD0D6FA04DB8E0D29E6330D,
	U3CTransSceneU3Ed__10_System_Collections_IEnumerator_get_Current_mF14EC66728AD2648CAC780E643FF20AE0E9D0914,
	AppleCatchManager_Start_mA7454944A5868CE61B5BD6A17C5D0BBCC2E44589,
	AppleCatchManager_FixedUpdate_m1B3449F813C01C2E4068D4968479646FBA2CA509,
	AppleCatchManager_ItemInstanceSetParameter_m9FEFEAE7183F4713C4ED573DCB507755C2891B20,
	AppleCatchManager_GameSetStart_m37C5A1C5444E858A570CB2FA57F4FAF953590DBC,
	AppleCatchManager_ItemInstance_mEDD19EF2FC76E833A0DCE15204CF7E98369C786A,
	AppleCatchManager_GetApple_mA597A63B94F1300C4E7272DC3E38A2813D0314FA,
	AppleCatchManager_GetInsect_m5C8B71705C07BEAA97EC960F232BF5DAB9AACFC5,
	AppleCatchManager_OnClickBackButton_m320AA1B9A9C72B40D1DDDF2CA3C3CA0B9B1B6AD9,
	AppleCatchManager_OnClickReStartButton_m9F85E502FB4B8255F9D16E204D3E4DBB4A0F6B53,
	AppleCatchManager_ReStartButtonActive_m482340B56FC27737AD635C5BFE0C6C1E80E2B0A0,
	AppleCatchManager__ctor_mDEFD24CA16BCF531B5E5EE3FD118DDEA36D1CBD7,
	AppleCatchPlayerController_Start_mCD599400153614623A6DD00CC71006678F8B6E59,
	AppleCatchPlayerController_OnCollisionEnter2D_mBDF7DCC95A254E7966D55C7DB296FF5F145C726A,
	AppleCatchPlayerController_OnMouseDrag_mE7A2F423B18848E150E093056CB0C274CAF2D5DF,
	AppleCatchPlayerController__ctor_m1795AEB17CC7E18C571FAA7F91BF8A6D94E96EBB,
	AppleCatchStageSlectManager_Start_m6888DAC4A82958EC25A3D39808610F344DBD5F68,
	AppleCatchStageSlectManager_OnClickSelectButton_m604EE1AE6FD2B8753164BCE3992BE8EFBEFA0879,
	AppleCatchStageSlectManager_OnClickBackButton_mDA720852F329AFEFB324A413411339872FE49D28,
	AppleCatchStageSlectManager__ctor_m54DA6BA5060DC6E3E58701F452E9BA60504F5AD2,
	ItemPrefabController_FixedUpdate_m249A9BA46B5D5D194AD4671EFE5941387416B907,
	ItemPrefabController_ItemPrefabControll_mA80B74C8A248DF3E9FA82F06A28D42F07A5C678D,
	ItemPrefabController__ctor_m2481493864C55C12ECC59DF16CA80807C9EF188F,
	CameraStableAspect_Awake_m8BB1F059EB334778D8241BF71B81865FA18C9139,
	CameraStableAspect_Update_m16BA8EAA8AAF545332B9468A1A8D56182394510C,
	CameraStableAspect_UpdateCameraWithCheck_m33767755B9E5FCA6B25261067A97868E2265A33F,
	CameraStableAspect_UpdateCamera_mD088EA93E094AF45BAB9D7E790164A3F470B1556,
	CameraStableAspect__ctor_mB3FBCB60546A52BB2B2297003BD3E273EDBF29C0,
	RectScalerWithViewport_Awake_mFA199E5E1D6C1EA5F7EC6270FAF78A39B96AE011,
	RectScalerWithViewport_Update_m76273C7F0EF5FFD8ADCA193D252E938801F6AB24,
	RectScalerWithViewport_OnValidate_mE45284F8A4AFDF2C6E7055F5EE91403F8D3E5B4C,
	RectScalerWithViewport_UpdateRectWithCheck_m4CC8EF1FFB35DB6D1A174E58971A45CE406F7DAB,
	RectScalerWithViewport_UpdateRect_m4FA8D4B76027E1EBB04493D8B5EE60A2F88538A9,
	RectScalerWithViewport__ctor_m61613DD9F4F9422F089F94EA7ED44084985499BA,
	ColorMatchingStageSlectManager_Start_m349A961200EDD46EDC7337ACA02790620B856861,
	ColorMatchingStageSlectManager_Update_mE8EB93B613543276CD60AE2C504BCBAB59908002,
	ColorMatchingStageSlectManager_PushStageSelectButton_m2BBDBA62C6448BD5434F4F58F3FA6E6474A10A6B,
	ColorMatchingStageSlectManager_BackButton_mB83E54677D069B5E69377FDE057744436649AEA0,
	ColorMatchingStageSlectManager_GobackStageSelect_m028422CADC90DBF5FFF829C4E9868EF5B873D739,
	ColorMatchingStageSlectManager__ctor_mBEF5EC4D6223D31932DA6EFDE786717703182798,
	CourseSelectManager_Start_mCFB41539F608FFD4C26446E3F53262E6D92D6A8A,
	CourseSelectManager_OnClickStageSelectButton_m954D82A504F80B1A48C05D264C410980B08A087A,
	CourseSelectManager_OnClickBackButton_m1706C1AC1AE656A1284FFF64088887F9518699A6,
	CourseSelectManager__ctor_m5F1ED0AA2079CA6C3AF431F86BF389D85A383960,
	GoForTheGoalStageSlectManager_Start_mA559068A80277788B67E5BB7201BA51A986DAEB8,
	GoForTheGoalStageSlectManager_OnClickStageSelectButton_m0A00CE28E9D028F5AF98DA86431CAD73F2180E2F,
	GoForTheGoalStageSlectManager_OnClickBackButton_m92483CAD7EEF032C70352A04A89396C772FBCEDC,
	GoForTheGoalStageSlectManager__ctor_m1A228B973AAB16BBDBF53E1A62C14F7441A757BB,
	PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF,
	PlayerController_Update_m1F4051EB5BCBCCE5EEE2E3E49B7E278C3B14EC33,
	PlayerController_OnCollisionEnter2D_m28C67E4361403BA9990C1E6D9526F78362591667,
	PlayerController_OnTriggerEnter2D_mB82BB8D400DC3E0E6B7117AE1A94E72E99CF53FB,
	PlayerController_PlayerSpeedPower_mC2D8CA83587680EA80269954CB1D59A93A3F71F1,
	PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33,
	MoveBaseManager_OnMouseDrag_mE8378022FC2F0C6DDA8FB362CABF22DA70080775,
	MoveBaseManager__ctor_mD1F65D2189C2448890BB8CE47C5261AF5DE41E9B,
	PalmAndGoalGameManager_Start_m93D80E951B34EDB1D23FC4804AEFA865E9370100,
	PalmAndGoalGameManager_OnClickGoButton_m95360ED9A3BB4D09A25B33570492486F37088ED3,
	PalmAndGoalGameManager_OnClickRetryButton_m21A41CABF66A119EF1F8B7660098FD68A9EDBCDE,
	PalmAndGoalGameManager_OnClickGoalBackButton_m2B76FFA3D60982253E0ED7B5521225EE63E5174C,
	PalmAndGoalGameManager_OnClickPalmBackButton_mE5D41ECEF572861480AFCD6D7C66C7D5484DEEEA,
	PalmAndGoalGameManager_StageClear_m314CAFCF5362DAFC6AD952948FEF5E2ABD7D276D,
	PalmAndGoalGameManager__ctor_mA9C4F2F968785B32406D61BF1F3AC8C862EE550C,
	PalmFruitCatchStageSlectManager_Start_m3D4F4F67820CA1AA0295ECDA86C39AE1DCAEB686,
	PalmFruitCatchStageSlectManager_OnClickStageSelectButton_mAD40F2FD27B9E30A1515EB80E6304E2F609AA14A,
	PalmFruitCatchStageSlectManager_OnClickBackButton_m2E193CA26F5199C718E7122B54FD5E9975703A18,
	PalmFruitCatchStageSlectManager__ctor_m9F8A4396354BED45BDC7A5B36F7027394177BA3C,
	PalmFruitlManager_OnCollisionEnter2D_mF063608C7F3BC77F82DE57EDD1AC417956E4C7EA,
	PalmFruitlManager_OnTriggerEnter2D_mC009BA8948279EF6CF5B650249D4D0D9F08C20D5,
	PalmFruitlManager__ctor_m939CE278E957EF1BA8E40621C3B1F985E3FBD004,
	MatchingManager_Start_m88ED67277DA452A820943869A60379D61E310F74,
	MatchingManager_Update_m4CD0E2179BFE9271568B603303CADA1DD48B042D,
	MatchingManager_OnClickShapeBackButton_mE64C4F8754DC04631AF2B7652540154F492314E1,
	MatchingManager_OnClickSoundBackButton_m5CB7F7785DDC1B7A9E3AA5F471EE7C81B583A86A,
	MatchingManager_OnClickColorBackButton_mA4613602FD110386371C90F7C823870548ABB08A,
	MatchingManager_OnClickThingsBackButton_m05F2D7D19FA4DFD69D4C1B66413069EBDB1F7183,
	MatchingManager_SuccessShape1_mE39D26BA6C29599E8FD2DFE3CAE80E0749123EE1,
	MatchingManager_SuccessShape2_m59DD23C7D8414EDD4AE843B2849F00437D8562B0,
	MatchingManager_SuccessShape3_m65594F4D1618A49EF9EE9DDCD4040034F68EB2F3,
	MatchingManager_SuccessShape4_mE476FA557FFC7ED0E928AC0ABB118DBE61EB26D2,
	MatchingManager_AllSuccesGameClear_m1513CA9D97BEB8293C1A4B3D33BAEE9F0893F74E,
	MatchingManager_OnClickSoundButton1_m494BB4B7E18027AD7C6C1EA166D44C1F1ECE10D8,
	MatchingManager_OnClickSoundButton2_m595BA36E1B8EF05D5FA1F5CE2629F1E17E951D40,
	MatchingManager_OnClickSoundButton3_mAEC35AA6461917ABDA97B0060C1D297F4F55652D,
	MatchingManager_OnClickSoundButton4_m912585168E1835344F4DA7AD973A735FFA61FD30,
	MatchingManager__ctor_mF042FF850EA53DFAFE25508BC9CA297D2F7C1D42,
	ShapeController1_Start_mDD5CC39FB3D459FBDEB472BAA9D8CEDF8B700872,
	ShapeController1_Update_m5C2AC821891A25F948A9F6E9807E5B4A9D5F3D14,
	ShapeController1_ShapeSuccesClear_m7968F143468C05209655AE675F6E70ED66C63DAD,
	ShapeController1_ShapeParticle_mBA4E5F47494B5848F7FDED83BE3EE9B89404A7D1,
	ShapeController1_OnMouseDrag_m95B73E06BD4D358E884AE4A5053C0EBD11D8D595,
	ShapeController1__ctor_mDDFE884B945177EBEAE05DEB9418209A7241AB56,
	ShapeController2_Start_m8A79AF5212F608435E6ABE253418811B2F7A2FAC,
	ShapeController2_Update_mF2E7C6E884E4E9C77B0DC11F8CEDB23BB1C0AA42,
	ShapeController2_ShapeSuccesClear_m94469D9F43BC87043828EE3D79BC5E9B7E2A47E8,
	ShapeController2_ShapeParticle_mFB50597B7DB5517A92C1423A2C5B8042F80B629B,
	ShapeController2_OnMouseDrag_mCBDA60AF2F1A44DE14DC85BF953AF864B1A12162,
	ShapeController2__ctor_mCA53A78BA95EAE8699DD6F19387CE292E768A810,
	ShapeController3_Start_m19528F95B9E01CAC470E1C62C32C25A3883D36D0,
	ShapeController3_Update_m0436D2609766AE2C582144C5DFF413E3D78E9551,
	ShapeController3_ShapeSuccesClear_mC1736059C9E735A467B8BBEA0B0C2D3ADAE77A02,
	ShapeController3_ShapeParticle_mBFEA5B7E1CFC6BD6F1AABD7A9962AC8F3B2FA576,
	ShapeController3_OnMouseDrag_mE282E7BB1731816EAA78E4D4F7219DED0C2F9ED0,
	ShapeController3__ctor_m3C4FE6ED11431F36223AA09682E37526E30D9B6A,
	ShapeController4_Start_m9483E323A13D9A302430732A19A88B91DE10C928,
	ShapeController4_Update_mA8768F6C2E5A927F59F6147384C45C17E0652090,
	ShapeController4_ShapeSuccesClear_m741C9BAC7B0D08E3F81B369654C138991558BD63,
	ShapeController4_ShapeParticle_m946EE310F989E95159EAAD1906B07F2FEF2EF88A,
	ShapeController4_OnMouseDrag_mF1C79A50AC442DC5D6BCED4806DDCF6AC19F02FE,
	ShapeController4__ctor_m6FE848AE78127721F848D4666CDA8E20F059CD14,
	CardController_get_isSelected_m925DFAAD2D2EB0328859E1F44DE4781A58054C6F,
	CardController_Set_mCF3346623A8E669C129630E3CF28D28EA36B2127,
	CardController_OnClick_mD92C25AAA7E20330387721CDDAB4E00284B9E057,
	CardController_onRotate_m41A23FF3DBCB6CB642F622DC9AB6AA289595931F,
	CardController_onReturnRotate_m9C5BA2C62CE87EE4ECC0D35C9F5E6AAB3000D386,
	CardController_SetHide_mBC2818BE5851FA2255B7EED3795CFA26C0D59B03,
	CardController_SetInvisible_m2ABEEDDD0FBB92D7B38CC836ED47DBA2A5BC741C,
	CardController__ctor_m3593D3890C1A596631E325FB2C9C07D68364980D,
	CardController_U3COnClickU3Eb__9_0_mF225D7032A108B302B15A8E373D6AB15959B1F67,
	CardController_U3COnClickU3Eb__9_1_m383EFE5F7A686E3FE4B5BDB288F89FDCBE1F4577,
	CardController_U3CSetHideU3Eb__12_0_m06136A22B90E8092BB5F8DFC9EC9D4C19C3BE29B,
	U3CU3Ec__DisplayClass10_0__ctor_m949970D641C88B9D0B54FA2F0513BC91F9E496E8,
	U3CU3Ec__DisplayClass10_0_U3ConRotateU3Eb__0_m28206B02A681E27CC17085697A0CA1BB06A16B0A,
	U3CU3Ec__DisplayClass11_0__ctor_m0AF819C3403D8A5EA4352DD9AC2AE4D970FF8964,
	U3CU3Ec__DisplayClass11_0_U3ConReturnRotateU3Eb__0_m58891DB77BE41BCBE3586C753A8A1A2223167225,
	U3CU3Ec__cctor_m4AFCC61DC6DC0879C8695AC71B2C2AAFD182A60F,
	U3CU3Ec__ctor_m10BACD9520D150E638331AB180D71D221E5D4D39,
	U3CU3Ec_U3CSetHideU3Eb__12_1_m12DB67AE49512D74BF7B3FC50DBA43E477E89239,
	CardData_get_Id_m2F2F67A6B6E18F66DD2884A47404A5AA6DF47742,
	CardData_set_Id_mB775FDEC373EFC4D8B45C369F09E9A6675EAAB40,
	CardData_get_ImgSprite_m29E9FB86757F8F381F72665D70055737F78A41EA,
	CardData_set_ImgSprite_mD7E883ED6BE6E724E96F76C33002EDD330F655CD,
	CardData__ctor_mAB5EBF5189FB88C264087C89B198DF5DF8C0F095,
	CardCreateManager_Start_m6414124036CC51428862AA4FE25D72622203C220,
	CardCreateManager_GameContentsNumber_m878F478C787B80F9658F532CAE7F04E18FA1A685,
	CardCreateManager_RandumCardPlacement_m83C2612095918D79E1F76050FDB7C3997B0063A6,
	CardCreateManager_CreateCard_mFD8D52AC87FC95F0536C57D8666885FA850F789D,
	CardCreateManager_HideCardList_mCDC58856D24B2B7FB871C45EC45FB7B39504A26B,
	CardCreateManager_mSetDealCardAnime_mD75460D7D0EE5E72FF049A48D5036462C73D9CFF,
	CardCreateManager__ctor_m10D96A568093E863926FF95F6A97EA03F5142B7B,
	U3CU3Ec__cctor_m7198AA25F46049460F7FE7F0183C75D3B90C8630,
	U3CU3Ec__ctor_m14E82AE95EBD7E6EE5D44A1D23B58620B987DEDB,
	U3CU3Ec_U3CRandumCardPlacementU3Eb__12_0_mB8351D0F9DD9389312DD41FE0DAEC00208B4FFE9,
	U3CU3Ec_U3CRandumCardPlacementU3Eb__12_1_m6D46EC79F65155C21B80437A0946B3789333C64E,
	U3CU3Ec_U3CCreateCardU3Eb__13_0_m9976E972D56AAEDF0DD8F2DBC42E12650E24A2AD,
	U3CU3Ec__DisplayClass15_0__ctor_m50CB1CB4BBF4EA21159C6D7C0A025608599A061D,
	U3CU3Ec__DisplayClass15_0_U3CmSetDealCardAnimeU3Eb__0_m0327DBE8CF7E23DEB9D66E5FD48DAB3438536720,
	GameSceneManager_Awake_m98F814B7C0D760AEC43A4BA6B71439B010E885CD,
	GameSceneManager_Update_mF84FC3FDB1756D98B7BBEFC5B5023F2E376F07F3,
	GameSceneManager_GameContentsNumber_m4A424968A5B4A92C86BA1200409F77AB4C5B5888,
	GameSceneManager_GameContents_mD82863EB0954603BCDF8EAD44476E2B8B452EE00,
	GameSceneManager_OnClickBackButton_m03341802C465BF3E0399A7BD80C47284CE6E90C4,
	GameSceneManager__ctor_m8BDD7367124B10E28EAB106035B280F1C6CE979E,
	GameStateController_get_Instance_mB97B49A7497739799E2DF04DF9D3DB0745D3B047,
	GameStateController__ctor_m31EB272531625F3334844A6A0213D29ABE98DA18,
	MemoryWeaknessStageSlectManager_Start_mFC5A3CF9D47F43A8F6B40D197FD72ECA9F5C967E,
	MemoryWeaknessStageSlectManager_OnClickStageSelectButton_mC5C587F0ADAA65B27BEEA257702AC5966A8DBF28,
	MemoryWeaknessStageSlectManager_OnClickBackButton_mC9EF587E373C410C9EDB2F37FAD220251A165912,
	MemoryWeaknessStageSlectManager__ctor_m27921BA8D6BFB3E618915B8103CF1A109A001290,
	PrecautionaryStatementSceneChange_Start_m44D212320F0A63AC8FFE8B4634568E4E8C300FF1,
	PrecautionaryStatementSceneChange_TitleSceneChange_mB7DD17D17BD1D0F208DF68AB5F74550FFA8317A9,
	PrecautionaryStatementSceneChange__ctor_m0960C353E75697994DFB76C3F824A91AA8E185D0,
	ShapeMatchingStageSlectManager_Start_m53304B2A2B71CBA48E1AA910D65D3CCD4D22CAF4,
	ShapeMatchingStageSlectManager_PushStageSelectButton_mF32F6FA61E3186F251D23771B628A9655B1313BA,
	ShapeMatchingStageSlectManager_BackButton_m12073D6F2D71B2E40A035F35B81F50E9F46682DE,
	ShapeMatchingStageSlectManager_GobackStageSelect_m018AB02974C242E871E4343122B7D54E0937B5A5,
	ShapeMatchingStageSlectManager__ctor_m728CC91A62081ACE6E8A93397B12E40E30FC9AD8,
	SoundMatchingStageSlectManager_Start_m36BBF19E4C9EDD28FCE70E628087003033BA3925,
	SoundMatchingStageSlectManager_Update_m308042517A8C6FD15B62A39CD4C5F5D510EF12E8,
	SoundMatchingStageSlectManager_PushStageSelectButton_m87C8D135699D2D86B578A4DCEDC3A64DE3797C73,
	SoundMatchingStageSlectManager_BackButton_m8C3264F63977A8AB98C1B21765AB436117C70974,
	SoundMatchingStageSlectManager_GobackStageSelect_mCBF75B86811F3A0FE56D9838FB1D4F1AC32DFF2B,
	SoundMatchingStageSlectManager__ctor_mAA823E24E1D807F75E28EF1906B30E84919122A1,
	ThingsMatchingStageSlectManager_Start_mFC83FB0B4365222BE74C0C220548EDBCCB737B96,
	ThingsMatchingStageSlectManager_Update_mADAB051E88DB1058C9F1EB665D81524326D2D894,
	ThingsMatchingStageSlectManager_PushStageSelectButton_m989D61D574C3F8BCD35F64036F88C24CE8E9497B,
	ThingsMatchingStageSlectManager_BackButton_m65C2D2634BD5CB59C18E9A259CD6F6DCB47ED0B6,
	ThingsMatchingStageSlectManager_GobackStageSelect_m110F6EAF5F42BABFE6B16924FB0D7EC0B72AA5FE,
	ThingsMatchingStageSlectManager__ctor_mB31EFAC1A023B0B33B1D8FA0753B3C706EBE45DE,
	ExitConfirmationManager_Start_mA7E1E6E4BB2FED39AADEA36BED338C588038A1F1,
	ExitConfirmationManager_Update_mAAA2A9AAE8FEE7F74F93019944C661F5279EC474,
	ExitConfirmationManager_SuccessShape1_m91312147112F0F3F67E41C2B41DEB3CB78285615,
	ExitConfirmationManager_SuccessShape2_m4A06AAFF75B56549A88E1698F12C7648C13B7D30,
	ExitConfirmationManager_SuccessShape3_m4F7F23E0D7406589960654C875D62035AB1BF5A2,
	ExitConfirmationManager_AllSuccesExitView_m8B6348A1D541E0FDAD953D09AFC0D7A32E74A606,
	ExitConfirmationManager__ctor_m4C1187A9F2DC3B96E1AEA40BAF937281DB23788C,
	MainSoundManager_Awake_mCEC847FF5E831E30543705B07B35AED81D6C969B,
	MainSoundManager_Start_m22F9FE4B2F0E499F3772C63B200BAD8A702E2315,
	MainSoundManager_SoundNumberSet_m89D86506005562DCCCF09F6031086D9E851613DD,
	MainSoundManager_OnActiveSceneChanged_mA63D4801527858B0C3D11781B04EFF77E7EB8C80,
	MainSoundManager__ctor_mF02C439B9FB0BC61A6111EF7C7B2FCFDF73213A0,
	TextTitle_FixedUpdate_mF917F50586A2FE97FE7F773F0DDD737521D408DE,
	TextTitle_SetTextColorChange_mE8006EF7517CA9C064258ECD7B157735F3D91BD4,
	TextTitle__ctor_mA49159D4FF806BCF45C0DCACAD905C171F041714,
	TextTitle__cctor_m14037CE87C9B0E55B70F20C3BB50D7746A550DFC,
	TitleManager_Start_m5EB0BAEFA202C6B62649F77BDE8213F8FE56F282,
	TitleManager_OnClickStartButton_mA217718CF58C65F0BB2DEB6ACCC376D536CC3475,
	TitleManager_OnClickCreditViewButton_m95F943C1066DB6F26C2D62A90A352DC66A5C77C6,
	TitleManager_OnClickCreditViewBackButton_m2337ACCA9C77D903ABC4FA9565F5F134921C7B69,
	TitleManager_OnClickExitViewButton_m7C891B3DADB202D8B2C7D803C853C60A4CB023ED,
	TitleManager_OnClickExitButton_m94EABA61A584F75C223D21BB5F6F19CCD0C0CBCF,
	TitleManager_OnClickonfirmationViewBackButton_mFAF43718343DF3E02B7E2AA0EE137F370B32AB44,
	TitleManager__ctor_m08406351E7FEB77F80D0A3A7F3FCC58D5CAAE237,
};
static const int32_t s_InvokerIndices[216] = 
{
	3418,
	3418,
	5333,
	3418,
	3418,
	1654,
	1294,
	3418,
	2808,
	3418,
	3274,
	3336,
	3418,
	3336,
	3418,
	3418,
	1679,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	2825,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	2825,
	2825,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	2825,
	2825,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3274,
	2825,
	3418,
	2825,
	2825,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	5359,
	3418,
	3418,
	3317,
	2808,
	3336,
	2825,
	1535,
	3418,
	3418,
	3418,
	3418,
	2825,
	3418,
	3418,
	5359,
	3418,
	2239,
	2239,
	2239,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	5333,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	3418,
	2808,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	1676,
	3418,
	3418,
	4851,
	3418,
	5359,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	216,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
