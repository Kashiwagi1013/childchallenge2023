﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AppleCatchManager::Start()
extern void AppleCatchManager_Start_m00F030A55849CF94C768972E89F63B72A8795503 (void);
// 0x00000002 System.Void AppleCatchManager::FixedUpdate()
extern void AppleCatchManager_FixedUpdate_mB3EA9F3B881C116942F8124F844D8349CF7579D5 (void);
// 0x00000003 System.Void AppleCatchManager::ItemInstanceSetParameter(System.Single,System.Int32)
extern void AppleCatchManager_ItemInstanceSetParameter_m06978782DABFEDB03824B0E1759CA0A1B9D3F2E6 (void);
// 0x00000004 System.Void AppleCatchManager::GameSetStart()
extern void AppleCatchManager_GameSetStart_m0B780FE272CE1315142B96E4C03BF6C33E6E6D66 (void);
// 0x00000005 System.Void AppleCatchManager::ItemInstance()
extern void AppleCatchManager_ItemInstance_m8278F223190CC49A687152190E44154057A802F3 (void);
// 0x00000006 System.Void AppleCatchManager::GetApple()
extern void AppleCatchManager_GetApple_mFBCE0E6CFFB080D1091D1829F3B4553EBB478051 (void);
// 0x00000007 System.Void AppleCatchManager::GetInsect()
extern void AppleCatchManager_GetInsect_m6CDF74CF95882A769ABA7344DF070D1A514EFFFE (void);
// 0x00000008 System.Void AppleCatchManager::OnClickBackButton()
extern void AppleCatchManager_OnClickBackButton_mB56FC9C421753F6082BBECB399110862CB4A5FBD (void);
// 0x00000009 System.Void AppleCatchManager::OnClickReStartButton(System.Int32)
extern void AppleCatchManager_OnClickReStartButton_m45F913F20D832CF25866322E450003DE63ED5298 (void);
// 0x0000000A System.Void AppleCatchManager::ReStartButtonActive()
extern void AppleCatchManager_ReStartButtonActive_m36AEC1D56B54C50BC4927DE4981B2592D47E10F1 (void);
// 0x0000000B System.Void AppleCatchManager::.ctor()
extern void AppleCatchManager__ctor_mFBEE16EED2CD0C4EA509D9FB5AB22A77F6A3E63D (void);
// 0x0000000C System.Void AppleCatchPlayerController::Start()
extern void AppleCatchPlayerController_Start_mFCCC3DDA9EC13BAD43A60A3C893C83F106B057DE (void);
// 0x0000000D System.Void AppleCatchPlayerController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void AppleCatchPlayerController_OnCollisionEnter2D_mF056391FFE6533D9E51B2046FF048D9794436A26 (void);
// 0x0000000E System.Void AppleCatchPlayerController::OnMouseDrag()
extern void AppleCatchPlayerController_OnMouseDrag_mA52CB9F3C09E987FAB2AF05AB7D4489502B8F372 (void);
// 0x0000000F System.Void AppleCatchPlayerController::.ctor()
extern void AppleCatchPlayerController__ctor_m5F507CC4C7648BB3BA814E53D7EB9BE7D403489A (void);
// 0x00000010 System.Void AppleCatchStageSlectManager::Start()
extern void AppleCatchStageSlectManager_Start_m8C40229216CFE557CAB8988CB747F8F6504E88D0 (void);
// 0x00000011 System.Void AppleCatchStageSlectManager::OnClickSelectButton(System.Int32)
extern void AppleCatchStageSlectManager_OnClickSelectButton_m0F8A48B500A86E6607F926A3E0A6CDF440DC15F4 (void);
// 0x00000012 System.Void AppleCatchStageSlectManager::OnClickBackButton()
extern void AppleCatchStageSlectManager_OnClickBackButton_mE8F6256530B9C743E153012D3E3803BDD83FAD4B (void);
// 0x00000013 System.Void AppleCatchStageSlectManager::.ctor()
extern void AppleCatchStageSlectManager__ctor_mD8FF897A51BD74519D491592CF25CAC8C3096E9A (void);
// 0x00000014 System.Void ItemPrefabController::FixedUpdate()
extern void ItemPrefabController_FixedUpdate_m9671A5750E4DC0C58050C46C23CD40F8832006B7 (void);
// 0x00000015 System.Void ItemPrefabController::ItemPrefabControll()
extern void ItemPrefabController_ItemPrefabControll_m60D9A302963DEF29E47E9AA7695CB076A324D49A (void);
// 0x00000016 System.Void ItemPrefabController::.ctor()
extern void ItemPrefabController__ctor_mB21DE380C1A8AC8EDD37F71E7693B1D903FD3BBD (void);
// 0x00000017 System.Void CameraStableAspect::Awake()
extern void CameraStableAspect_Awake_m328D6571775AEFF6993BE24FE734979086FB8687 (void);
// 0x00000018 System.Void CameraStableAspect::Update()
extern void CameraStableAspect_Update_m0D7795A14032553D43ACFC3DC393A98DDC5B80E1 (void);
// 0x00000019 System.Void CameraStableAspect::UpdateCameraWithCheck()
extern void CameraStableAspect_UpdateCameraWithCheck_m0949C2CB80D052FF1EAB4D496090678644963454 (void);
// 0x0000001A System.Void CameraStableAspect::UpdateCamera()
extern void CameraStableAspect_UpdateCamera_m2E5DB180254849B6439EC7F597399DC8E7B2FCEC (void);
// 0x0000001B System.Void CameraStableAspect::.ctor()
extern void CameraStableAspect__ctor_m4BE559EF693F2DA43187D23FBFB07B3BCB02E32C (void);
// 0x0000001C System.Void RectScalerWithViewport::Awake()
extern void RectScalerWithViewport_Awake_mD0D368A8C0D013B88B81027F5A5EC67951D4E6BD (void);
// 0x0000001D System.Void RectScalerWithViewport::Update()
extern void RectScalerWithViewport_Update_mE97AD04D19B70C4449B0C1AC2049CDD2E58C6E27 (void);
// 0x0000001E System.Void RectScalerWithViewport::OnValidate()
extern void RectScalerWithViewport_OnValidate_m5D9895F0531DE903047AA7A517A14F38E12641CD (void);
// 0x0000001F System.Void RectScalerWithViewport::UpdateRectWithCheck()
extern void RectScalerWithViewport_UpdateRectWithCheck_m39195108C053C9C1CAD1DA041CD1AD27117F4DBF (void);
// 0x00000020 System.Void RectScalerWithViewport::UpdateRect()
extern void RectScalerWithViewport_UpdateRect_mE476B0588D3034F6D83CD9BDC4ADDCE5C9D95D2A (void);
// 0x00000021 System.Void RectScalerWithViewport::.ctor()
extern void RectScalerWithViewport__ctor_mA78BD8F1D1929A8C3B8D0DF2E170D65A58D480D5 (void);
// 0x00000022 System.Void ColorMatchingStageSlectManager::Start()
extern void ColorMatchingStageSlectManager_Start_m1B4530276A846BE544A6A8AB816F33C8FF1F6F42 (void);
// 0x00000023 System.Void ColorMatchingStageSlectManager::Update()
extern void ColorMatchingStageSlectManager_Update_m512868F1CABC75D60F6495BB5D0E3654DD4017A4 (void);
// 0x00000024 System.Void ColorMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ColorMatchingStageSlectManager_PushStageSelectButton_m9264456CF6FD84B867C4216AF13B7BB79C3A9BB7 (void);
// 0x00000025 System.Void ColorMatchingStageSlectManager::BackButton()
extern void ColorMatchingStageSlectManager_BackButton_m965392FB9A0836F4EAC948F94DDD9FD16A5F4D47 (void);
// 0x00000026 System.Void ColorMatchingStageSlectManager::GobackStageSelect()
extern void ColorMatchingStageSlectManager_GobackStageSelect_m7CF8EE45F0C643AF66640A64B97DFC640A9AD622 (void);
// 0x00000027 System.Void ColorMatchingStageSlectManager::.ctor()
extern void ColorMatchingStageSlectManager__ctor_m24D6C06FE8F7403332F0861DFD400D4BFC69CCF5 (void);
// 0x00000028 System.Void CourseSelectManager::Start()
extern void CourseSelectManager_Start_mC909E3540B93DD039FD3B96D821603BFCDD14C56 (void);
// 0x00000029 System.Void CourseSelectManager::OnClickStageSelectButton(System.Int32)
extern void CourseSelectManager_OnClickStageSelectButton_mF19DB4925FA3AF1B0CC360BAFD7E9CECA75836E3 (void);
// 0x0000002A System.Void CourseSelectManager::OnClickBackButton()
extern void CourseSelectManager_OnClickBackButton_mEB5B40E74C2C8833FF1C0871DF24410E8C09D598 (void);
// 0x0000002B System.Void CourseSelectManager::.ctor()
extern void CourseSelectManager__ctor_mF6B08B14494AD6BEDB1D1747689BF737FF1B56C3 (void);
// 0x0000002C System.Void GoForTheGoalStageSlectManager::Start()
extern void GoForTheGoalStageSlectManager_Start_mF64D450EE290F184E022CE2E59A868FF4A217627 (void);
// 0x0000002D System.Void GoForTheGoalStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void GoForTheGoalStageSlectManager_OnClickStageSelectButton_m88592EB7E70276CB70CB007E20DC67A44E2235C1 (void);
// 0x0000002E System.Void GoForTheGoalStageSlectManager::OnClickBackButton()
extern void GoForTheGoalStageSlectManager_OnClickBackButton_m7D2C5E64E2BD73EF006896C61810492829AFB677 (void);
// 0x0000002F System.Void GoForTheGoalStageSlectManager::.ctor()
extern void GoForTheGoalStageSlectManager__ctor_m06CBA228DE298163BD684F0A36EEA1E04010EF3A (void);
// 0x00000030 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x00000031 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000032 System.Void PlayerController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerController_OnCollisionEnter2D_m9A113567D03E9BFF74D8D1959B8C88124E03BBDE (void);
// 0x00000033 System.Void PlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerController_OnTriggerEnter2D_m1B1E3F94F29560C6CD4687C6556D74A092CC672E (void);
// 0x00000034 System.Void PlayerController::PlayerSpeedPower()
extern void PlayerController_PlayerSpeedPower_m1F47DE4C1E9F6E6DDA49602E94335AFDA4DD994C (void);
// 0x00000035 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000036 System.Void MoveBaseManager::OnMouseDrag()
extern void MoveBaseManager_OnMouseDrag_mA931D58672558EE89FFD05FDC5D98BD081FE2824 (void);
// 0x00000037 System.Void MoveBaseManager::.ctor()
extern void MoveBaseManager__ctor_m80983AD6D1672469E5669DB94F295767A5E564D5 (void);
// 0x00000038 System.Void PalmAndGoalGameManager::Start()
extern void PalmAndGoalGameManager_Start_mF6A54E5A2129B6760BAF8F6472DE35AFA00171EB (void);
// 0x00000039 System.Void PalmAndGoalGameManager::OnClickGoButton()
extern void PalmAndGoalGameManager_OnClickGoButton_m8FDF80809449812745993275F724903FD6C72A38 (void);
// 0x0000003A System.Void PalmAndGoalGameManager::OnClickRetryButton()
extern void PalmAndGoalGameManager_OnClickRetryButton_m126EE3EF45ED34CA25CF22C0C259E501F61EB383 (void);
// 0x0000003B System.Void PalmAndGoalGameManager::OnClickGoalBackButton()
extern void PalmAndGoalGameManager_OnClickGoalBackButton_m7A8F0D1A3AB8352B234DCCD1367A97D7B865D816 (void);
// 0x0000003C System.Void PalmAndGoalGameManager::OnClickPalmBackButton()
extern void PalmAndGoalGameManager_OnClickPalmBackButton_m89DBDA7DDB1A99F3871C444E7134A77CF184594A (void);
// 0x0000003D System.Void PalmAndGoalGameManager::StageClear()
extern void PalmAndGoalGameManager_StageClear_m3A0C0E79DF79F8FC1BB1215699FF6EF2752ED643 (void);
// 0x0000003E System.Void PalmAndGoalGameManager::.ctor()
extern void PalmAndGoalGameManager__ctor_mA3A0D7FE8A47E06E27E2F8FAB7350A2464A192D4 (void);
// 0x0000003F System.Void PalmFruitCatchStageSlectManager::Start()
extern void PalmFruitCatchStageSlectManager_Start_mD971C9D0109436E9E20089F17920628DCDD9BA2D (void);
// 0x00000040 System.Void PalmFruitCatchStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void PalmFruitCatchStageSlectManager_OnClickStageSelectButton_m04BA945F6995102C2A5DFC562A446E9A19989832 (void);
// 0x00000041 System.Void PalmFruitCatchStageSlectManager::OnClickBackButton()
extern void PalmFruitCatchStageSlectManager_OnClickBackButton_m70F2C3FFB087F1824A85C538903DEA08A294CDD1 (void);
// 0x00000042 System.Void PalmFruitCatchStageSlectManager::.ctor()
extern void PalmFruitCatchStageSlectManager__ctor_m9A27CAF6A1B10CC16F8EFDA7F8309B3BF11BB2F3 (void);
// 0x00000043 System.Void PalmFruitlManager::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PalmFruitlManager_OnCollisionEnter2D_m4BD7CFB81C84C2802725552BF73546E59B200D56 (void);
// 0x00000044 System.Void PalmFruitlManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PalmFruitlManager_OnTriggerEnter2D_m840B6F6F9FCE51150F86CBDC787B59A4DB00BE34 (void);
// 0x00000045 System.Void PalmFruitlManager::.ctor()
extern void PalmFruitlManager__ctor_m781858815B3F3EA541EA9C60E24295F23C09289B (void);
// 0x00000046 System.Void MatchingManager::Start()
extern void MatchingManager_Start_m2B8FA56B55C9FF0C7BA2446B0C6C428EA32E835C (void);
// 0x00000047 System.Void MatchingManager::Update()
extern void MatchingManager_Update_m6A7FF50BB9D4AE95E5D545E8F2D0ABD1F2D091E0 (void);
// 0x00000048 System.Void MatchingManager::OnClickShapeBackButton()
extern void MatchingManager_OnClickShapeBackButton_mAAB2ECDA6CE65B6DEDC345A829855B15429363FB (void);
// 0x00000049 System.Void MatchingManager::OnClickSoundBackButton()
extern void MatchingManager_OnClickSoundBackButton_m9AACB18C2F0EBADA27E9854C3013A02A90B3CB54 (void);
// 0x0000004A System.Void MatchingManager::OnClickColorBackButton()
extern void MatchingManager_OnClickColorBackButton_mDAF6195833CD20178F66BD9DAC3625F558A6E811 (void);
// 0x0000004B System.Void MatchingManager::OnClickThingsBackButton()
extern void MatchingManager_OnClickThingsBackButton_m1FD7E84C74E951402D7829C908362EF99D41F5E0 (void);
// 0x0000004C System.Void MatchingManager::SuccessShape1()
extern void MatchingManager_SuccessShape1_m97000FDB2DFD74D662C8A6DC34773D403015C836 (void);
// 0x0000004D System.Void MatchingManager::SuccessShape2()
extern void MatchingManager_SuccessShape2_mD6AAEE8E6FBE80CEB03E55C0035255CCA794FDD8 (void);
// 0x0000004E System.Void MatchingManager::SuccessShape3()
extern void MatchingManager_SuccessShape3_m3D79D43A6C528E24F431413AF71606C2AD0E6D7C (void);
// 0x0000004F System.Void MatchingManager::SuccessShape4()
extern void MatchingManager_SuccessShape4_m775584BDCB12762E76EC2EF5B14C41EBE4DD468C (void);
// 0x00000050 System.Void MatchingManager::AllSuccesGameClear()
extern void MatchingManager_AllSuccesGameClear_m36D08D43C61FD9367266978B3391C7E65EBA7688 (void);
// 0x00000051 System.Void MatchingManager::OnClickSoundButton1()
extern void MatchingManager_OnClickSoundButton1_m9F61DB0E35D00FD1F839D465FF42509388B6B123 (void);
// 0x00000052 System.Void MatchingManager::OnClickSoundButton2()
extern void MatchingManager_OnClickSoundButton2_m38F02DA74A5938FD1F5A73B2917C14A968A9F737 (void);
// 0x00000053 System.Void MatchingManager::OnClickSoundButton3()
extern void MatchingManager_OnClickSoundButton3_mC3D9370EEA142DA5CB0F40511036BFD2D6DB23FD (void);
// 0x00000054 System.Void MatchingManager::OnClickSoundButton4()
extern void MatchingManager_OnClickSoundButton4_m936E21FBB7369C41341F825D3F698CD303BA4910 (void);
// 0x00000055 System.Void MatchingManager::.ctor()
extern void MatchingManager__ctor_m64D0D7D32B0E45F3B9FB20BA00E27BA1F227D825 (void);
// 0x00000056 System.Void ShapeController1::Start()
extern void ShapeController1_Start_mD83DC41CB01CA0E1551ACF6BDF9BB3AD64233D87 (void);
// 0x00000057 System.Void ShapeController1::Update()
extern void ShapeController1_Update_m7E5272C26838F740EE3E3F88ADD7213BE0118AF9 (void);
// 0x00000058 System.Void ShapeController1::ShapeSuccesClear()
extern void ShapeController1_ShapeSuccesClear_m3DC11DE388EF239B8525768D39F6625E7AF83A92 (void);
// 0x00000059 System.Void ShapeController1::ShapeParticle()
extern void ShapeController1_ShapeParticle_mD974804AF04BC34BEC1DB90E3E7B9A148938CB8F (void);
// 0x0000005A System.Void ShapeController1::OnMouseDrag()
extern void ShapeController1_OnMouseDrag_m4BFAC06B77B274B3EBB4A0B2C5588C59C0D0823A (void);
// 0x0000005B System.Void ShapeController1::.ctor()
extern void ShapeController1__ctor_m35E3FD9F2EC6059CD78241C3ED5A0995ED02E56D (void);
// 0x0000005C System.Void ShapeController2::Start()
extern void ShapeController2_Start_mC047D8B23807A58375834896956B18D4E3488918 (void);
// 0x0000005D System.Void ShapeController2::Update()
extern void ShapeController2_Update_m945DCC8D5515B2C57782B46472AFAEDAEE7FCF6A (void);
// 0x0000005E System.Void ShapeController2::ShapeSuccesClear()
extern void ShapeController2_ShapeSuccesClear_mC3235BA0AAE6B1843CF1BC772B8B0B0645D2F3AA (void);
// 0x0000005F System.Void ShapeController2::ShapeParticle()
extern void ShapeController2_ShapeParticle_m42B76D164469050D2C517B1D361ECF14DED02C20 (void);
// 0x00000060 System.Void ShapeController2::OnMouseDrag()
extern void ShapeController2_OnMouseDrag_m87A9B3F025CBBFE2D225656218EF81956F914FEB (void);
// 0x00000061 System.Void ShapeController2::.ctor()
extern void ShapeController2__ctor_mF2CE426D3FAAC29DE1B3F058189BBB3B0866D203 (void);
// 0x00000062 System.Void ShapeController3::Start()
extern void ShapeController3_Start_m48AE27216C45C02752296DC42374307523FBDFE2 (void);
// 0x00000063 System.Void ShapeController3::Update()
extern void ShapeController3_Update_mEE8D18C92C41870CFD142CAF478674E71429866A (void);
// 0x00000064 System.Void ShapeController3::ShapeSuccesClear()
extern void ShapeController3_ShapeSuccesClear_m78B3CE24771C87F8F216157B7326B22CAE127D22 (void);
// 0x00000065 System.Void ShapeController3::ShapeParticle()
extern void ShapeController3_ShapeParticle_m2628108CE18A5E37FA72B21CA250E2BE77402A77 (void);
// 0x00000066 System.Void ShapeController3::OnMouseDrag()
extern void ShapeController3_OnMouseDrag_m669B47FB40A00D5E981C4C23A1806C5BFD7DB47F (void);
// 0x00000067 System.Void ShapeController3::.ctor()
extern void ShapeController3__ctor_m96A532A9B9C05462757925F948F4626943AACF13 (void);
// 0x00000068 System.Void ShapeController4::Start()
extern void ShapeController4_Start_m4D4ED6F5289598E4534CEAEA155963FD268AC329 (void);
// 0x00000069 System.Void ShapeController4::Update()
extern void ShapeController4_Update_mEB838B6DF230C7D503A6FFC260EB35395E95F9AE (void);
// 0x0000006A System.Void ShapeController4::ShapeSuccesClear()
extern void ShapeController4_ShapeSuccesClear_mE9E275C96FA8965C6CBB521B5DE2A282A9A554A5 (void);
// 0x0000006B System.Void ShapeController4::ShapeParticle()
extern void ShapeController4_ShapeParticle_mC3A3C84481681110B778C0B8FBA0FF49DBF21A32 (void);
// 0x0000006C System.Void ShapeController4::OnMouseDrag()
extern void ShapeController4_OnMouseDrag_mE02B3ED8958E1A2217B609B96ADF1C2330270B20 (void);
// 0x0000006D System.Void ShapeController4::.ctor()
extern void ShapeController4__ctor_mF12377C7B8E80DD8762145AB29E055873C6BFDB5 (void);
// 0x0000006E System.Boolean CardController::get_isSelected()
extern void CardController_get_isSelected_m2EE7D7AAFB816D9ACD2553CB3DD96B7E97F090FE (void);
// 0x0000006F System.Void CardController::Set(CardData)
extern void CardController_Set_mD7650866CB1B44E89525340FFBF2DC7182406009 (void);
// 0x00000070 System.Void CardController::OnClick()
extern void CardController_OnClick_mC7E856A5CCF60712DA507B454F6658C9B0C4FBFC (void);
// 0x00000071 System.Void CardController::onRotate(System.Action)
extern void CardController_onRotate_mDBE86F25B025F33367BEE2498553C7939263F8A5 (void);
// 0x00000072 System.Void CardController::onReturnRotate(System.Action)
extern void CardController_onReturnRotate_m0E335B2C90828740C6F701AF5F7EC152E016A61E (void);
// 0x00000073 System.Void CardController::SetHide()
extern void CardController_SetHide_mE1D1EB19E802F8DAF90474CBFBA8FD53F251CAF6 (void);
// 0x00000074 System.Void CardController::SetInvisible()
extern void CardController_SetInvisible_mB512A5D25FEEECE60538BE93A7A38A175839BE49 (void);
// 0x00000075 System.Void CardController::.ctor()
extern void CardController__ctor_m558A28CDE4691CF299F1D1653133D5813A74877C (void);
// 0x00000076 System.Void CardController::<OnClick>b__9_0()
extern void CardController_U3COnClickU3Eb__9_0_m8D4A65D65A6F28A5D279411D4348DFAE3326297D (void);
// 0x00000077 System.Void CardController::<OnClick>b__9_1()
extern void CardController_U3COnClickU3Eb__9_1_m8465D5C6C3A97075AC45B81ED1FEE5C367009C6B (void);
// 0x00000078 System.Void CardController::<SetHide>b__12_0()
extern void CardController_U3CSetHideU3Eb__12_0_mBE248453E218422B1254CA634D0533E60FAE4078 (void);
// 0x00000079 System.Void CardController/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mD275808DFBE5388584B51E02794F03EA684306CF (void);
// 0x0000007A System.Void CardController/<>c__DisplayClass10_0::<onRotate>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3ConRotateU3Eb__0_m164304990B1C6F1C0B8066BA1D698A68B745CAF5 (void);
// 0x0000007B System.Void CardController/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m5A7E1C98094A9FF4C749E0C9571AB4FA45C9F5A5 (void);
// 0x0000007C System.Void CardController/<>c__DisplayClass11_0::<onReturnRotate>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3ConReturnRotateU3Eb__0_m3CAC725BFB40CF0DC9AFB70E80608E1CFA342091 (void);
// 0x0000007D System.Void CardController/<>c::.cctor()
extern void U3CU3Ec__cctor_mAF2074CFE2F7D39A9E683A13633FA43A9FB45D44 (void);
// 0x0000007E System.Void CardController/<>c::.ctor()
extern void U3CU3Ec__ctor_mE214F859A65B14507805792E9D95D0A2401CEC35 (void);
// 0x0000007F System.Void CardController/<>c::<SetHide>b__12_1()
extern void U3CU3Ec_U3CSetHideU3Eb__12_1_mC9EBE254F8CC3618ECCDDB622EDD15E35FA09450 (void);
// 0x00000080 System.Int32 CardData::get_Id()
extern void CardData_get_Id_m79C0DFC788946A981DB3E1FA480DE099B2FB35A9 (void);
// 0x00000081 System.Void CardData::set_Id(System.Int32)
extern void CardData_set_Id_m1A121A8AF1CD87FB6CE05C8AE8871706D04B6F10 (void);
// 0x00000082 UnityEngine.Sprite CardData::get_ImgSprite()
extern void CardData_get_ImgSprite_m9A01FDD254D8312E4C17DD6CE0D9AF3A4E1422BC (void);
// 0x00000083 System.Void CardData::set_ImgSprite(UnityEngine.Sprite)
extern void CardData_set_ImgSprite_m2B5A71E5F58E144312E1B78CC1FF599788142D7A (void);
// 0x00000084 System.Void CardData::.ctor(System.Int32,UnityEngine.Sprite)
extern void CardData__ctor_mF441869FFEFE73244DAFF4392A58C042336669A9 (void);
// 0x00000085 System.Void CardCreateManager::Start()
extern void CardCreateManager_Start_mC6D6AB9DC8C5B969CD4E407C6DEEA9D356AE7A21 (void);
// 0x00000086 System.Void CardCreateManager::GameContentsNumber()
extern void CardCreateManager_GameContentsNumber_mCED5C8941E403345BF0DBB5CB7EDFAE6F16B4B79 (void);
// 0x00000087 System.Void CardCreateManager::RandumCardPlacement()
extern void CardCreateManager_RandumCardPlacement_m8C59836ABE4C15CEE77309958A8FCA0E8A9846F3 (void);
// 0x00000088 System.Void CardCreateManager::CreateCard()
extern void CardCreateManager_CreateCard_mF0EBE3B9B53BB0B5A7EB82022F851661FA732F6D (void);
// 0x00000089 System.Void CardCreateManager::HideCardList(System.Collections.Generic.List`1<System.Int32>)
extern void CardCreateManager_HideCardList_m189656D61ED424C379C120153BA47356DF9ED842 (void);
// 0x0000008A System.Void CardCreateManager::mSetDealCardAnime()
extern void CardCreateManager_mSetDealCardAnime_mAECC64E28FA8A0E7A89B4D18E96ECF9DA4D124CA (void);
// 0x0000008B System.Void CardCreateManager::.ctor()
extern void CardCreateManager__ctor_m207EDC8F2611226BEF9A00808711683FCCA8AB53 (void);
// 0x0000008C System.Void CardCreateManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m557BEC7A8BE57FD695BEAE4831B08E726D993CFF (void);
// 0x0000008D System.Void CardCreateManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mB2D14533A3CE332391F4DAB37B24A7D6C8926DAE (void);
// 0x0000008E System.Guid CardCreateManager/<>c::<RandumCardPlacement>b__12_0(CardData)
extern void U3CU3Ec_U3CRandumCardPlacementU3Eb__12_0_m314280F477F6AFC40822FDB718ED3A363B5D35B0 (void);
// 0x0000008F System.Guid CardCreateManager/<>c::<RandumCardPlacement>b__12_1(CardData)
extern void U3CU3Ec_U3CRandumCardPlacementU3Eb__12_1_m4AA6E9AEC8F341EFFD7641F7220E7CFB97B1F2AB (void);
// 0x00000090 System.Guid CardCreateManager/<>c::<CreateCard>b__13_0(CardData)
extern void U3CU3Ec_U3CCreateCardU3Eb__13_0_m15B2C2B3184F8FEE1EF0B1845D139014602F468F (void);
// 0x00000091 System.Void CardCreateManager/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m0851DACAD184F8568E11027C02CEFECE5E72FAAD (void);
// 0x00000092 System.Void CardCreateManager/<>c__DisplayClass15_0::<mSetDealCardAnime>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CmSetDealCardAnimeU3Eb__0_m192C1F13ED112A2E5D69840CBC8DCDF348D422F4 (void);
// 0x00000093 System.Void GameSceneManager::Awake()
extern void GameSceneManager_Awake_m7C08A7A70F8F21EEF2AE081D2AC92D5B04C8BBDF (void);
// 0x00000094 System.Void GameSceneManager::Update()
extern void GameSceneManager_Update_mC61782FEF964C4BE165E3220291576151EC1E58E (void);
// 0x00000095 System.Void GameSceneManager::GameContentsNumber()
extern void GameSceneManager_GameContentsNumber_m8CEF764FE1D473097CFDAFCC98A94A07AC006312 (void);
// 0x00000096 System.Void GameSceneManager::GameContents()
extern void GameSceneManager_GameContents_mACC99FDF03FC2570162BB4DDCB27CB200A929424 (void);
// 0x00000097 System.Void GameSceneManager::OnClickBackButton()
extern void GameSceneManager_OnClickBackButton_mB7B7D31ED796783A1A338E20C798AC9CF999B321 (void);
// 0x00000098 System.Void GameSceneManager::.ctor()
extern void GameSceneManager__ctor_m9CC1DE29E8061EA5605EC80F57CC247F29460E3A (void);
// 0x00000099 GameStateController GameStateController::get_Instance()
extern void GameStateController_get_Instance_mDAFC76BF7E3A1E317CEBD96C63F29139A55FF027 (void);
// 0x0000009A System.Void GameStateController::.ctor()
extern void GameStateController__ctor_m7D811B594F00B45A5D723FBB51F4163A4EA52971 (void);
// 0x0000009B System.Void MemoryWeaknessStageSlectManager::Start()
extern void MemoryWeaknessStageSlectManager_Start_m3409BACA387D519E90162A75A410303730D62A20 (void);
// 0x0000009C System.Void MemoryWeaknessStageSlectManager::OnClickStageSelectButton(System.Int32)
extern void MemoryWeaknessStageSlectManager_OnClickStageSelectButton_m09DF4E80C6CC0036BA66ED59D2A2C604311AEA3C (void);
// 0x0000009D System.Void MemoryWeaknessStageSlectManager::OnClickBackButton()
extern void MemoryWeaknessStageSlectManager_OnClickBackButton_mEB11DC14223B03D9550A33A38BD072B274FC3965 (void);
// 0x0000009E System.Void MemoryWeaknessStageSlectManager::.ctor()
extern void MemoryWeaknessStageSlectManager__ctor_m109CB8A16896C125447960DAF4E2FFE48EDB1644 (void);
// 0x0000009F System.Void PrecautionaryStatementSceneChange::Start()
extern void PrecautionaryStatementSceneChange_Start_mF4FB57304939E269FDACD45B2459FE25F186B243 (void);
// 0x000000A0 System.Void PrecautionaryStatementSceneChange::TitleSceneChange()
extern void PrecautionaryStatementSceneChange_TitleSceneChange_mECA8086A493CF5BF504D855B2CF1A0FFDCDD546B (void);
// 0x000000A1 System.Void PrecautionaryStatementSceneChange::.ctor()
extern void PrecautionaryStatementSceneChange__ctor_mBC50B3D41ECB07557174DF1F90EE8F17422D8B63 (void);
// 0x000000A2 System.Void ShapeMatchingStageSlectManager::Start()
extern void ShapeMatchingStageSlectManager_Start_m1F3EA07BE68EBA680435CCB5678D5174D7B07C7E (void);
// 0x000000A3 System.Void ShapeMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ShapeMatchingStageSlectManager_PushStageSelectButton_mAA24BC28D55304315E6EFB89CF67EF61A6014EB6 (void);
// 0x000000A4 System.Void ShapeMatchingStageSlectManager::BackButton()
extern void ShapeMatchingStageSlectManager_BackButton_m7C7ED483B83F56A8EB949327471536CE15E6610C (void);
// 0x000000A5 System.Void ShapeMatchingStageSlectManager::GobackStageSelect()
extern void ShapeMatchingStageSlectManager_GobackStageSelect_m96008D8E0BD1DEE67EF7CD3E11558AB51D42DC84 (void);
// 0x000000A6 System.Void ShapeMatchingStageSlectManager::.ctor()
extern void ShapeMatchingStageSlectManager__ctor_m689520FFAD92A1D5BA2A687273B27C388E72681D (void);
// 0x000000A7 System.Void SoundMatchingStageSlectManager::Start()
extern void SoundMatchingStageSlectManager_Start_m8FB3F051465A90DE0F9B0B0C50CE6EAE64745F81 (void);
// 0x000000A8 System.Void SoundMatchingStageSlectManager::Update()
extern void SoundMatchingStageSlectManager_Update_mF8F37ECD1D6D3793542CD3A78E88C2D039C269B2 (void);
// 0x000000A9 System.Void SoundMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void SoundMatchingStageSlectManager_PushStageSelectButton_m86E1CCECC35BB32804E5ECD9A05A36FF75BEFA55 (void);
// 0x000000AA System.Void SoundMatchingStageSlectManager::BackButton()
extern void SoundMatchingStageSlectManager_BackButton_mAF4129BA7DF2448BF1F6C2624695E9AEEB2409B1 (void);
// 0x000000AB System.Void SoundMatchingStageSlectManager::GobackStageSelect()
extern void SoundMatchingStageSlectManager_GobackStageSelect_m5B217BB3F66E4459F580E1442FA71D2E5C0E9A4D (void);
// 0x000000AC System.Void SoundMatchingStageSlectManager::.ctor()
extern void SoundMatchingStageSlectManager__ctor_mA8F3E02C19AD1B724A832023F1E03FBEE656F997 (void);
// 0x000000AD System.Void ThingsMatchingStageSlectManager::Start()
extern void ThingsMatchingStageSlectManager_Start_mFBA368D85D78108F267B427CDAF6395399755EB4 (void);
// 0x000000AE System.Void ThingsMatchingStageSlectManager::Update()
extern void ThingsMatchingStageSlectManager_Update_mC09616DDFB3CD69FC429FE355B6E0A816B3D69FF (void);
// 0x000000AF System.Void ThingsMatchingStageSlectManager::PushStageSelectButton(System.Int32)
extern void ThingsMatchingStageSlectManager_PushStageSelectButton_mB4F3887040A20DDE4347D4877A272BCC7D601F9C (void);
// 0x000000B0 System.Void ThingsMatchingStageSlectManager::BackButton()
extern void ThingsMatchingStageSlectManager_BackButton_m0A650DA5BE330A018504E78D8C33582CFAFC5033 (void);
// 0x000000B1 System.Void ThingsMatchingStageSlectManager::GobackStageSelect()
extern void ThingsMatchingStageSlectManager_GobackStageSelect_m72D0DCF678931B58C502C0062B02AC9A8D5EB11C (void);
// 0x000000B2 System.Void ThingsMatchingStageSlectManager::.ctor()
extern void ThingsMatchingStageSlectManager__ctor_mADE2C0A22EE6C4DA74F21D20AAE52627D55059DA (void);
// 0x000000B3 System.Void ExitConfirmationManager::Start()
extern void ExitConfirmationManager_Start_mC2E2DE167A1D469C359BA969D810962119D58440 (void);
// 0x000000B4 System.Void ExitConfirmationManager::Update()
extern void ExitConfirmationManager_Update_mC54F126D6650701E6BF1DB3221E820A3E619C2ED (void);
// 0x000000B5 System.Void ExitConfirmationManager::SuccessShape1()
extern void ExitConfirmationManager_SuccessShape1_mBACD2B4EED0DFEE807430462FDF75A7A6C992E4D (void);
// 0x000000B6 System.Void ExitConfirmationManager::SuccessShape2()
extern void ExitConfirmationManager_SuccessShape2_m40B9AB2E38A7C74AF1DA95F68B87F3C3A9DB4AE6 (void);
// 0x000000B7 System.Void ExitConfirmationManager::SuccessShape3()
extern void ExitConfirmationManager_SuccessShape3_m061D4AEE6543D3DA02AA75F53053C009916A37D9 (void);
// 0x000000B8 System.Void ExitConfirmationManager::AllSuccesExitView()
extern void ExitConfirmationManager_AllSuccesExitView_m708A7236794E4F7FA432F40C6EEF3A0E8F4020DD (void);
// 0x000000B9 System.Void ExitConfirmationManager::.ctor()
extern void ExitConfirmationManager__ctor_mF41474A670D507CC8FE04901EB9725EFBD220755 (void);
// 0x000000BA System.Void MainSoundManager::Awake()
extern void MainSoundManager_Awake_m21E4EAA37E79B9F403EE42FB3115A1A1B267E872 (void);
// 0x000000BB System.Void MainSoundManager::Start()
extern void MainSoundManager_Start_m16F11F5B57933005D90A8B7DE56D42DFCBE8F91A (void);
// 0x000000BC System.Void MainSoundManager::SoundNumberSet()
extern void MainSoundManager_SoundNumberSet_m8A975F2BF4B1D11D16EDEA545DC3B7F17BA9456B (void);
// 0x000000BD System.Void MainSoundManager::OnActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void MainSoundManager_OnActiveSceneChanged_mF8A3705C360D0684B88F6B653B70EB87052F403F (void);
// 0x000000BE System.Void MainSoundManager::.ctor()
extern void MainSoundManager__ctor_m755FFBE3FEE4A12AAC925F02C08CABC6E84C2C48 (void);
// 0x000000BF System.Void TextTitle::FixedUpdate()
extern void TextTitle_FixedUpdate_m125D9C715016147503512EF45D7653B83D4D0681 (void);
// 0x000000C0 System.Void TextTitle::SetTextColorChange(System.Boolean,UnityEngine.UI.Text)
extern void TextTitle_SetTextColorChange_mA2B6CD979E840AA99787B032FA72A5265C3F3355 (void);
// 0x000000C1 System.Void TextTitle::.ctor()
extern void TextTitle__ctor_m3328E19BF8F770D3481C17FC205781995B0183C6 (void);
// 0x000000C2 System.Void TextTitle::.cctor()
extern void TextTitle__cctor_m67CC85E97CC19D4F675049C8B459077D9BD4855A (void);
// 0x000000C3 System.Void TitleManager::Start()
extern void TitleManager_Start_mB1BDED26067331336418D8C8F760161E62F96224 (void);
// 0x000000C4 System.Void TitleManager::OnClickStartButton()
extern void TitleManager_OnClickStartButton_mAE0E8C999D96DDB07EDE7065495C951B8508BD82 (void);
// 0x000000C5 System.Void TitleManager::OnClickCreditViewButton()
extern void TitleManager_OnClickCreditViewButton_mAA55F9528124BCE57C3A724C88E81E50EE84DF03 (void);
// 0x000000C6 System.Void TitleManager::OnClickCreditViewBackButton()
extern void TitleManager_OnClickCreditViewBackButton_m04448F6C93A6E15ED4CFB478264A1BB1B3B5FBC7 (void);
// 0x000000C7 System.Void TitleManager::OnClickExitViewButton()
extern void TitleManager_OnClickExitViewButton_m2AE4B6848A261EB0BCA8CCF31B6C9253E6B576D5 (void);
// 0x000000C8 System.Void TitleManager::OnClickExitButton()
extern void TitleManager_OnClickExitButton_m920A603C290827F8EF609890434BB9CF1B2AFD7D (void);
// 0x000000C9 System.Void TitleManager::OnClickonfirmationViewBackButton()
extern void TitleManager_OnClickonfirmationViewBackButton_m7B9A48C3230E9F9189F236215019D0696F902701 (void);
// 0x000000CA System.Void TitleManager::.ctor()
extern void TitleManager__ctor_m0D14731249E3B6D0846A587A421CAE4FAD2056D0 (void);
// 0x000000CB System.Void FadeSample::FadeScene()
extern void FadeSample_FadeScene_mB54F893E8697CFEC34DE4ED8D5B4B8A5FD96E5A0 (void);
// 0x000000CC System.Void FadeSample::.ctor()
extern void FadeSample__ctor_m95FC1B59D71C060AD8997C68F0FFB39D75BA2F66 (void);
// 0x000000CD FadeManager FadeManager::get_Instance()
extern void FadeManager_get_Instance_m84B96C58D7866421F4D1163F11898ABAF4A1B16B (void);
// 0x000000CE System.Void FadeManager::Awake()
extern void FadeManager_Awake_m16D26A3DA9BB3B787D2300FDB70F42C6842FEF25 (void);
// 0x000000CF System.Void FadeManager::OnGUI()
extern void FadeManager_OnGUI_m3369F47915E0911552EDF7B322D0A9EF0EC8EF55 (void);
// 0x000000D0 System.Void FadeManager::LoadScene(System.String,System.Single)
extern void FadeManager_LoadScene_mB26A77D4C299937DDE5CB538F75E028CFA7DDA2C (void);
// 0x000000D1 System.Collections.IEnumerator FadeManager::TransScene(System.String,System.Single)
extern void FadeManager_TransScene_m0B9411C1EF47A4519C554E3E69948EB9C1F2A2C5 (void);
// 0x000000D2 System.Void FadeManager::.ctor()
extern void FadeManager__ctor_m52ABCAEC678567697CEEB5286F6EF69EB652D2CA (void);
// 0x000000D3 System.Void FadeManager/<TransScene>d__10::.ctor(System.Int32)
extern void U3CTransSceneU3Ed__10__ctor_mF6B239297A0DBD79C3A40FA5D63E5FEB273848F3 (void);
// 0x000000D4 System.Void FadeManager/<TransScene>d__10::System.IDisposable.Dispose()
extern void U3CTransSceneU3Ed__10_System_IDisposable_Dispose_m618ACC8C0DC29159496C4A0361A7CC4DA9AC1B66 (void);
// 0x000000D5 System.Boolean FadeManager/<TransScene>d__10::MoveNext()
extern void U3CTransSceneU3Ed__10_MoveNext_mDFAF262ED8A3B8210DFA4B84630DD843120B768A (void);
// 0x000000D6 System.Object FadeManager/<TransScene>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97938FFBEC92B8F334896D95C41EADBBBFB93A5 (void);
// 0x000000D7 System.Void FadeManager/<TransScene>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTransSceneU3Ed__10_System_Collections_IEnumerator_Reset_m299EDF0D0B6B0ADB5C12E588426A661DB47118D0 (void);
// 0x000000D8 System.Object FadeManager/<TransScene>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTransSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m3CA3559639AF1EE9938E52B93C8072BF3CDEF1D9 (void);
static Il2CppMethodPointer s_methodPointers[216] = 
{
	AppleCatchManager_Start_m00F030A55849CF94C768972E89F63B72A8795503,
	AppleCatchManager_FixedUpdate_mB3EA9F3B881C116942F8124F844D8349CF7579D5,
	AppleCatchManager_ItemInstanceSetParameter_m06978782DABFEDB03824B0E1759CA0A1B9D3F2E6,
	AppleCatchManager_GameSetStart_m0B780FE272CE1315142B96E4C03BF6C33E6E6D66,
	AppleCatchManager_ItemInstance_m8278F223190CC49A687152190E44154057A802F3,
	AppleCatchManager_GetApple_mFBCE0E6CFFB080D1091D1829F3B4553EBB478051,
	AppleCatchManager_GetInsect_m6CDF74CF95882A769ABA7344DF070D1A514EFFFE,
	AppleCatchManager_OnClickBackButton_mB56FC9C421753F6082BBECB399110862CB4A5FBD,
	AppleCatchManager_OnClickReStartButton_m45F913F20D832CF25866322E450003DE63ED5298,
	AppleCatchManager_ReStartButtonActive_m36AEC1D56B54C50BC4927DE4981B2592D47E10F1,
	AppleCatchManager__ctor_mFBEE16EED2CD0C4EA509D9FB5AB22A77F6A3E63D,
	AppleCatchPlayerController_Start_mFCCC3DDA9EC13BAD43A60A3C893C83F106B057DE,
	AppleCatchPlayerController_OnCollisionEnter2D_mF056391FFE6533D9E51B2046FF048D9794436A26,
	AppleCatchPlayerController_OnMouseDrag_mA52CB9F3C09E987FAB2AF05AB7D4489502B8F372,
	AppleCatchPlayerController__ctor_m5F507CC4C7648BB3BA814E53D7EB9BE7D403489A,
	AppleCatchStageSlectManager_Start_m8C40229216CFE557CAB8988CB747F8F6504E88D0,
	AppleCatchStageSlectManager_OnClickSelectButton_m0F8A48B500A86E6607F926A3E0A6CDF440DC15F4,
	AppleCatchStageSlectManager_OnClickBackButton_mE8F6256530B9C743E153012D3E3803BDD83FAD4B,
	AppleCatchStageSlectManager__ctor_mD8FF897A51BD74519D491592CF25CAC8C3096E9A,
	ItemPrefabController_FixedUpdate_m9671A5750E4DC0C58050C46C23CD40F8832006B7,
	ItemPrefabController_ItemPrefabControll_m60D9A302963DEF29E47E9AA7695CB076A324D49A,
	ItemPrefabController__ctor_mB21DE380C1A8AC8EDD37F71E7693B1D903FD3BBD,
	CameraStableAspect_Awake_m328D6571775AEFF6993BE24FE734979086FB8687,
	CameraStableAspect_Update_m0D7795A14032553D43ACFC3DC393A98DDC5B80E1,
	CameraStableAspect_UpdateCameraWithCheck_m0949C2CB80D052FF1EAB4D496090678644963454,
	CameraStableAspect_UpdateCamera_m2E5DB180254849B6439EC7F597399DC8E7B2FCEC,
	CameraStableAspect__ctor_m4BE559EF693F2DA43187D23FBFB07B3BCB02E32C,
	RectScalerWithViewport_Awake_mD0D368A8C0D013B88B81027F5A5EC67951D4E6BD,
	RectScalerWithViewport_Update_mE97AD04D19B70C4449B0C1AC2049CDD2E58C6E27,
	RectScalerWithViewport_OnValidate_m5D9895F0531DE903047AA7A517A14F38E12641CD,
	RectScalerWithViewport_UpdateRectWithCheck_m39195108C053C9C1CAD1DA041CD1AD27117F4DBF,
	RectScalerWithViewport_UpdateRect_mE476B0588D3034F6D83CD9BDC4ADDCE5C9D95D2A,
	RectScalerWithViewport__ctor_mA78BD8F1D1929A8C3B8D0DF2E170D65A58D480D5,
	ColorMatchingStageSlectManager_Start_m1B4530276A846BE544A6A8AB816F33C8FF1F6F42,
	ColorMatchingStageSlectManager_Update_m512868F1CABC75D60F6495BB5D0E3654DD4017A4,
	ColorMatchingStageSlectManager_PushStageSelectButton_m9264456CF6FD84B867C4216AF13B7BB79C3A9BB7,
	ColorMatchingStageSlectManager_BackButton_m965392FB9A0836F4EAC948F94DDD9FD16A5F4D47,
	ColorMatchingStageSlectManager_GobackStageSelect_m7CF8EE45F0C643AF66640A64B97DFC640A9AD622,
	ColorMatchingStageSlectManager__ctor_m24D6C06FE8F7403332F0861DFD400D4BFC69CCF5,
	CourseSelectManager_Start_mC909E3540B93DD039FD3B96D821603BFCDD14C56,
	CourseSelectManager_OnClickStageSelectButton_mF19DB4925FA3AF1B0CC360BAFD7E9CECA75836E3,
	CourseSelectManager_OnClickBackButton_mEB5B40E74C2C8833FF1C0871DF24410E8C09D598,
	CourseSelectManager__ctor_mF6B08B14494AD6BEDB1D1747689BF737FF1B56C3,
	GoForTheGoalStageSlectManager_Start_mF64D450EE290F184E022CE2E59A868FF4A217627,
	GoForTheGoalStageSlectManager_OnClickStageSelectButton_m88592EB7E70276CB70CB007E20DC67A44E2235C1,
	GoForTheGoalStageSlectManager_OnClickBackButton_m7D2C5E64E2BD73EF006896C61810492829AFB677,
	GoForTheGoalStageSlectManager__ctor_m06CBA228DE298163BD684F0A36EEA1E04010EF3A,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_OnCollisionEnter2D_m9A113567D03E9BFF74D8D1959B8C88124E03BBDE,
	PlayerController_OnTriggerEnter2D_m1B1E3F94F29560C6CD4687C6556D74A092CC672E,
	PlayerController_PlayerSpeedPower_m1F47DE4C1E9F6E6DDA49602E94335AFDA4DD994C,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	MoveBaseManager_OnMouseDrag_mA931D58672558EE89FFD05FDC5D98BD081FE2824,
	MoveBaseManager__ctor_m80983AD6D1672469E5669DB94F295767A5E564D5,
	PalmAndGoalGameManager_Start_mF6A54E5A2129B6760BAF8F6472DE35AFA00171EB,
	PalmAndGoalGameManager_OnClickGoButton_m8FDF80809449812745993275F724903FD6C72A38,
	PalmAndGoalGameManager_OnClickRetryButton_m126EE3EF45ED34CA25CF22C0C259E501F61EB383,
	PalmAndGoalGameManager_OnClickGoalBackButton_m7A8F0D1A3AB8352B234DCCD1367A97D7B865D816,
	PalmAndGoalGameManager_OnClickPalmBackButton_m89DBDA7DDB1A99F3871C444E7134A77CF184594A,
	PalmAndGoalGameManager_StageClear_m3A0C0E79DF79F8FC1BB1215699FF6EF2752ED643,
	PalmAndGoalGameManager__ctor_mA3A0D7FE8A47E06E27E2F8FAB7350A2464A192D4,
	PalmFruitCatchStageSlectManager_Start_mD971C9D0109436E9E20089F17920628DCDD9BA2D,
	PalmFruitCatchStageSlectManager_OnClickStageSelectButton_m04BA945F6995102C2A5DFC562A446E9A19989832,
	PalmFruitCatchStageSlectManager_OnClickBackButton_m70F2C3FFB087F1824A85C538903DEA08A294CDD1,
	PalmFruitCatchStageSlectManager__ctor_m9A27CAF6A1B10CC16F8EFDA7F8309B3BF11BB2F3,
	PalmFruitlManager_OnCollisionEnter2D_m4BD7CFB81C84C2802725552BF73546E59B200D56,
	PalmFruitlManager_OnTriggerEnter2D_m840B6F6F9FCE51150F86CBDC787B59A4DB00BE34,
	PalmFruitlManager__ctor_m781858815B3F3EA541EA9C60E24295F23C09289B,
	MatchingManager_Start_m2B8FA56B55C9FF0C7BA2446B0C6C428EA32E835C,
	MatchingManager_Update_m6A7FF50BB9D4AE95E5D545E8F2D0ABD1F2D091E0,
	MatchingManager_OnClickShapeBackButton_mAAB2ECDA6CE65B6DEDC345A829855B15429363FB,
	MatchingManager_OnClickSoundBackButton_m9AACB18C2F0EBADA27E9854C3013A02A90B3CB54,
	MatchingManager_OnClickColorBackButton_mDAF6195833CD20178F66BD9DAC3625F558A6E811,
	MatchingManager_OnClickThingsBackButton_m1FD7E84C74E951402D7829C908362EF99D41F5E0,
	MatchingManager_SuccessShape1_m97000FDB2DFD74D662C8A6DC34773D403015C836,
	MatchingManager_SuccessShape2_mD6AAEE8E6FBE80CEB03E55C0035255CCA794FDD8,
	MatchingManager_SuccessShape3_m3D79D43A6C528E24F431413AF71606C2AD0E6D7C,
	MatchingManager_SuccessShape4_m775584BDCB12762E76EC2EF5B14C41EBE4DD468C,
	MatchingManager_AllSuccesGameClear_m36D08D43C61FD9367266978B3391C7E65EBA7688,
	MatchingManager_OnClickSoundButton1_m9F61DB0E35D00FD1F839D465FF42509388B6B123,
	MatchingManager_OnClickSoundButton2_m38F02DA74A5938FD1F5A73B2917C14A968A9F737,
	MatchingManager_OnClickSoundButton3_mC3D9370EEA142DA5CB0F40511036BFD2D6DB23FD,
	MatchingManager_OnClickSoundButton4_m936E21FBB7369C41341F825D3F698CD303BA4910,
	MatchingManager__ctor_m64D0D7D32B0E45F3B9FB20BA00E27BA1F227D825,
	ShapeController1_Start_mD83DC41CB01CA0E1551ACF6BDF9BB3AD64233D87,
	ShapeController1_Update_m7E5272C26838F740EE3E3F88ADD7213BE0118AF9,
	ShapeController1_ShapeSuccesClear_m3DC11DE388EF239B8525768D39F6625E7AF83A92,
	ShapeController1_ShapeParticle_mD974804AF04BC34BEC1DB90E3E7B9A148938CB8F,
	ShapeController1_OnMouseDrag_m4BFAC06B77B274B3EBB4A0B2C5588C59C0D0823A,
	ShapeController1__ctor_m35E3FD9F2EC6059CD78241C3ED5A0995ED02E56D,
	ShapeController2_Start_mC047D8B23807A58375834896956B18D4E3488918,
	ShapeController2_Update_m945DCC8D5515B2C57782B46472AFAEDAEE7FCF6A,
	ShapeController2_ShapeSuccesClear_mC3235BA0AAE6B1843CF1BC772B8B0B0645D2F3AA,
	ShapeController2_ShapeParticle_m42B76D164469050D2C517B1D361ECF14DED02C20,
	ShapeController2_OnMouseDrag_m87A9B3F025CBBFE2D225656218EF81956F914FEB,
	ShapeController2__ctor_mF2CE426D3FAAC29DE1B3F058189BBB3B0866D203,
	ShapeController3_Start_m48AE27216C45C02752296DC42374307523FBDFE2,
	ShapeController3_Update_mEE8D18C92C41870CFD142CAF478674E71429866A,
	ShapeController3_ShapeSuccesClear_m78B3CE24771C87F8F216157B7326B22CAE127D22,
	ShapeController3_ShapeParticle_m2628108CE18A5E37FA72B21CA250E2BE77402A77,
	ShapeController3_OnMouseDrag_m669B47FB40A00D5E981C4C23A1806C5BFD7DB47F,
	ShapeController3__ctor_m96A532A9B9C05462757925F948F4626943AACF13,
	ShapeController4_Start_m4D4ED6F5289598E4534CEAEA155963FD268AC329,
	ShapeController4_Update_mEB838B6DF230C7D503A6FFC260EB35395E95F9AE,
	ShapeController4_ShapeSuccesClear_mE9E275C96FA8965C6CBB521B5DE2A282A9A554A5,
	ShapeController4_ShapeParticle_mC3A3C84481681110B778C0B8FBA0FF49DBF21A32,
	ShapeController4_OnMouseDrag_mE02B3ED8958E1A2217B609B96ADF1C2330270B20,
	ShapeController4__ctor_mF12377C7B8E80DD8762145AB29E055873C6BFDB5,
	CardController_get_isSelected_m2EE7D7AAFB816D9ACD2553CB3DD96B7E97F090FE,
	CardController_Set_mD7650866CB1B44E89525340FFBF2DC7182406009,
	CardController_OnClick_mC7E856A5CCF60712DA507B454F6658C9B0C4FBFC,
	CardController_onRotate_mDBE86F25B025F33367BEE2498553C7939263F8A5,
	CardController_onReturnRotate_m0E335B2C90828740C6F701AF5F7EC152E016A61E,
	CardController_SetHide_mE1D1EB19E802F8DAF90474CBFBA8FD53F251CAF6,
	CardController_SetInvisible_mB512A5D25FEEECE60538BE93A7A38A175839BE49,
	CardController__ctor_m558A28CDE4691CF299F1D1653133D5813A74877C,
	CardController_U3COnClickU3Eb__9_0_m8D4A65D65A6F28A5D279411D4348DFAE3326297D,
	CardController_U3COnClickU3Eb__9_1_m8465D5C6C3A97075AC45B81ED1FEE5C367009C6B,
	CardController_U3CSetHideU3Eb__12_0_mBE248453E218422B1254CA634D0533E60FAE4078,
	U3CU3Ec__DisplayClass10_0__ctor_mD275808DFBE5388584B51E02794F03EA684306CF,
	U3CU3Ec__DisplayClass10_0_U3ConRotateU3Eb__0_m164304990B1C6F1C0B8066BA1D698A68B745CAF5,
	U3CU3Ec__DisplayClass11_0__ctor_m5A7E1C98094A9FF4C749E0C9571AB4FA45C9F5A5,
	U3CU3Ec__DisplayClass11_0_U3ConReturnRotateU3Eb__0_m3CAC725BFB40CF0DC9AFB70E80608E1CFA342091,
	U3CU3Ec__cctor_mAF2074CFE2F7D39A9E683A13633FA43A9FB45D44,
	U3CU3Ec__ctor_mE214F859A65B14507805792E9D95D0A2401CEC35,
	U3CU3Ec_U3CSetHideU3Eb__12_1_mC9EBE254F8CC3618ECCDDB622EDD15E35FA09450,
	CardData_get_Id_m79C0DFC788946A981DB3E1FA480DE099B2FB35A9,
	CardData_set_Id_m1A121A8AF1CD87FB6CE05C8AE8871706D04B6F10,
	CardData_get_ImgSprite_m9A01FDD254D8312E4C17DD6CE0D9AF3A4E1422BC,
	CardData_set_ImgSprite_m2B5A71E5F58E144312E1B78CC1FF599788142D7A,
	CardData__ctor_mF441869FFEFE73244DAFF4392A58C042336669A9,
	CardCreateManager_Start_mC6D6AB9DC8C5B969CD4E407C6DEEA9D356AE7A21,
	CardCreateManager_GameContentsNumber_mCED5C8941E403345BF0DBB5CB7EDFAE6F16B4B79,
	CardCreateManager_RandumCardPlacement_m8C59836ABE4C15CEE77309958A8FCA0E8A9846F3,
	CardCreateManager_CreateCard_mF0EBE3B9B53BB0B5A7EB82022F851661FA732F6D,
	CardCreateManager_HideCardList_m189656D61ED424C379C120153BA47356DF9ED842,
	CardCreateManager_mSetDealCardAnime_mAECC64E28FA8A0E7A89B4D18E96ECF9DA4D124CA,
	CardCreateManager__ctor_m207EDC8F2611226BEF9A00808711683FCCA8AB53,
	U3CU3Ec__cctor_m557BEC7A8BE57FD695BEAE4831B08E726D993CFF,
	U3CU3Ec__ctor_mB2D14533A3CE332391F4DAB37B24A7D6C8926DAE,
	U3CU3Ec_U3CRandumCardPlacementU3Eb__12_0_m314280F477F6AFC40822FDB718ED3A363B5D35B0,
	U3CU3Ec_U3CRandumCardPlacementU3Eb__12_1_m4AA6E9AEC8F341EFFD7641F7220E7CFB97B1F2AB,
	U3CU3Ec_U3CCreateCardU3Eb__13_0_m15B2C2B3184F8FEE1EF0B1845D139014602F468F,
	U3CU3Ec__DisplayClass15_0__ctor_m0851DACAD184F8568E11027C02CEFECE5E72FAAD,
	U3CU3Ec__DisplayClass15_0_U3CmSetDealCardAnimeU3Eb__0_m192C1F13ED112A2E5D69840CBC8DCDF348D422F4,
	GameSceneManager_Awake_m7C08A7A70F8F21EEF2AE081D2AC92D5B04C8BBDF,
	GameSceneManager_Update_mC61782FEF964C4BE165E3220291576151EC1E58E,
	GameSceneManager_GameContentsNumber_m8CEF764FE1D473097CFDAFCC98A94A07AC006312,
	GameSceneManager_GameContents_mACC99FDF03FC2570162BB4DDCB27CB200A929424,
	GameSceneManager_OnClickBackButton_mB7B7D31ED796783A1A338E20C798AC9CF999B321,
	GameSceneManager__ctor_m9CC1DE29E8061EA5605EC80F57CC247F29460E3A,
	GameStateController_get_Instance_mDAFC76BF7E3A1E317CEBD96C63F29139A55FF027,
	GameStateController__ctor_m7D811B594F00B45A5D723FBB51F4163A4EA52971,
	MemoryWeaknessStageSlectManager_Start_m3409BACA387D519E90162A75A410303730D62A20,
	MemoryWeaknessStageSlectManager_OnClickStageSelectButton_m09DF4E80C6CC0036BA66ED59D2A2C604311AEA3C,
	MemoryWeaknessStageSlectManager_OnClickBackButton_mEB11DC14223B03D9550A33A38BD072B274FC3965,
	MemoryWeaknessStageSlectManager__ctor_m109CB8A16896C125447960DAF4E2FFE48EDB1644,
	PrecautionaryStatementSceneChange_Start_mF4FB57304939E269FDACD45B2459FE25F186B243,
	PrecautionaryStatementSceneChange_TitleSceneChange_mECA8086A493CF5BF504D855B2CF1A0FFDCDD546B,
	PrecautionaryStatementSceneChange__ctor_mBC50B3D41ECB07557174DF1F90EE8F17422D8B63,
	ShapeMatchingStageSlectManager_Start_m1F3EA07BE68EBA680435CCB5678D5174D7B07C7E,
	ShapeMatchingStageSlectManager_PushStageSelectButton_mAA24BC28D55304315E6EFB89CF67EF61A6014EB6,
	ShapeMatchingStageSlectManager_BackButton_m7C7ED483B83F56A8EB949327471536CE15E6610C,
	ShapeMatchingStageSlectManager_GobackStageSelect_m96008D8E0BD1DEE67EF7CD3E11558AB51D42DC84,
	ShapeMatchingStageSlectManager__ctor_m689520FFAD92A1D5BA2A687273B27C388E72681D,
	SoundMatchingStageSlectManager_Start_m8FB3F051465A90DE0F9B0B0C50CE6EAE64745F81,
	SoundMatchingStageSlectManager_Update_mF8F37ECD1D6D3793542CD3A78E88C2D039C269B2,
	SoundMatchingStageSlectManager_PushStageSelectButton_m86E1CCECC35BB32804E5ECD9A05A36FF75BEFA55,
	SoundMatchingStageSlectManager_BackButton_mAF4129BA7DF2448BF1F6C2624695E9AEEB2409B1,
	SoundMatchingStageSlectManager_GobackStageSelect_m5B217BB3F66E4459F580E1442FA71D2E5C0E9A4D,
	SoundMatchingStageSlectManager__ctor_mA8F3E02C19AD1B724A832023F1E03FBEE656F997,
	ThingsMatchingStageSlectManager_Start_mFBA368D85D78108F267B427CDAF6395399755EB4,
	ThingsMatchingStageSlectManager_Update_mC09616DDFB3CD69FC429FE355B6E0A816B3D69FF,
	ThingsMatchingStageSlectManager_PushStageSelectButton_mB4F3887040A20DDE4347D4877A272BCC7D601F9C,
	ThingsMatchingStageSlectManager_BackButton_m0A650DA5BE330A018504E78D8C33582CFAFC5033,
	ThingsMatchingStageSlectManager_GobackStageSelect_m72D0DCF678931B58C502C0062B02AC9A8D5EB11C,
	ThingsMatchingStageSlectManager__ctor_mADE2C0A22EE6C4DA74F21D20AAE52627D55059DA,
	ExitConfirmationManager_Start_mC2E2DE167A1D469C359BA969D810962119D58440,
	ExitConfirmationManager_Update_mC54F126D6650701E6BF1DB3221E820A3E619C2ED,
	ExitConfirmationManager_SuccessShape1_mBACD2B4EED0DFEE807430462FDF75A7A6C992E4D,
	ExitConfirmationManager_SuccessShape2_m40B9AB2E38A7C74AF1DA95F68B87F3C3A9DB4AE6,
	ExitConfirmationManager_SuccessShape3_m061D4AEE6543D3DA02AA75F53053C009916A37D9,
	ExitConfirmationManager_AllSuccesExitView_m708A7236794E4F7FA432F40C6EEF3A0E8F4020DD,
	ExitConfirmationManager__ctor_mF41474A670D507CC8FE04901EB9725EFBD220755,
	MainSoundManager_Awake_m21E4EAA37E79B9F403EE42FB3115A1A1B267E872,
	MainSoundManager_Start_m16F11F5B57933005D90A8B7DE56D42DFCBE8F91A,
	MainSoundManager_SoundNumberSet_m8A975F2BF4B1D11D16EDEA545DC3B7F17BA9456B,
	MainSoundManager_OnActiveSceneChanged_mF8A3705C360D0684B88F6B653B70EB87052F403F,
	MainSoundManager__ctor_m755FFBE3FEE4A12AAC925F02C08CABC6E84C2C48,
	TextTitle_FixedUpdate_m125D9C715016147503512EF45D7653B83D4D0681,
	TextTitle_SetTextColorChange_mA2B6CD979E840AA99787B032FA72A5265C3F3355,
	TextTitle__ctor_m3328E19BF8F770D3481C17FC205781995B0183C6,
	TextTitle__cctor_m67CC85E97CC19D4F675049C8B459077D9BD4855A,
	TitleManager_Start_mB1BDED26067331336418D8C8F760161E62F96224,
	TitleManager_OnClickStartButton_mAE0E8C999D96DDB07EDE7065495C951B8508BD82,
	TitleManager_OnClickCreditViewButton_mAA55F9528124BCE57C3A724C88E81E50EE84DF03,
	TitleManager_OnClickCreditViewBackButton_m04448F6C93A6E15ED4CFB478264A1BB1B3B5FBC7,
	TitleManager_OnClickExitViewButton_m2AE4B6848A261EB0BCA8CCF31B6C9253E6B576D5,
	TitleManager_OnClickExitButton_m920A603C290827F8EF609890434BB9CF1B2AFD7D,
	TitleManager_OnClickonfirmationViewBackButton_m7B9A48C3230E9F9189F236215019D0696F902701,
	TitleManager__ctor_m0D14731249E3B6D0846A587A421CAE4FAD2056D0,
	FadeSample_FadeScene_mB54F893E8697CFEC34DE4ED8D5B4B8A5FD96E5A0,
	FadeSample__ctor_m95FC1B59D71C060AD8997C68F0FFB39D75BA2F66,
	FadeManager_get_Instance_m84B96C58D7866421F4D1163F11898ABAF4A1B16B,
	FadeManager_Awake_m16D26A3DA9BB3B787D2300FDB70F42C6842FEF25,
	FadeManager_OnGUI_m3369F47915E0911552EDF7B322D0A9EF0EC8EF55,
	FadeManager_LoadScene_mB26A77D4C299937DDE5CB538F75E028CFA7DDA2C,
	FadeManager_TransScene_m0B9411C1EF47A4519C554E3E69948EB9C1F2A2C5,
	FadeManager__ctor_m52ABCAEC678567697CEEB5286F6EF69EB652D2CA,
	U3CTransSceneU3Ed__10__ctor_mF6B239297A0DBD79C3A40FA5D63E5FEB273848F3,
	U3CTransSceneU3Ed__10_System_IDisposable_Dispose_m618ACC8C0DC29159496C4A0361A7CC4DA9AC1B66,
	U3CTransSceneU3Ed__10_MoveNext_mDFAF262ED8A3B8210DFA4B84630DD843120B768A,
	U3CTransSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97938FFBEC92B8F334896D95C41EADBBBFB93A5,
	U3CTransSceneU3Ed__10_System_Collections_IEnumerator_Reset_m299EDF0D0B6B0ADB5C12E588426A661DB47118D0,
	U3CTransSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m3CA3559639AF1EE9938E52B93C8072BF3CDEF1D9,
};
static const int32_t s_InvokerIndices[216] = 
{
	1361,
	1361,
	828,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1189,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1189,
	1189,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1189,
	1189,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1349,
	1189,
	1361,
	1189,
	1189,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	2399,
	1361,
	1361,
	1318,
	1180,
	1329,
	1189,
	770,
	1361,
	1361,
	1361,
	1361,
	1189,
	1361,
	1361,
	2399,
	1361,
	889,
	889,
	889,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	2384,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1361,
	1180,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	826,
	1361,
	1361,
	2201,
	1361,
	2399,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	1361,
	2384,
	1361,
	1361,
	816,
	628,
	1361,
	1180,
	1361,
	1349,
	1329,
	1361,
	1329,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	216,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
