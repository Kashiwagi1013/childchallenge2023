using UnityEngine;

public class PalmAndGoalGameManager : MonoBehaviour
{

    [SerializeField] GameObject playerPrefab;//リトライからプレイヤープレファブに置き換える用
    [SerializeField] GameObject player;//初期プレイヤー据え置き用
    [SerializeField] GameObject goButton;//ゴーボタン
    [SerializeField] GameObject retryButton;//リトライボタン
    [SerializeField] GameObject clearText;//クリア時テキスト
    [SerializeField] GameObject clearEfect;//ゲームクリア時エフェクト表示
    [SerializeField] GameObject[] moveBaseAnimation;//動く床アニメーション
    [SerializeField] GameObject backButtonAnimation;//クリア時バックボタンアニメーション
    [SerializeField] GameObject goalAnimation;//ゴールアニメーション
    [SerializeField] AudioClip clearSE1;//クリア時SE1
    [SerializeField] AudioClip clearSE2;//クリア時SE2
    [SerializeField] AudioClip backButtonSE;//バックボタンSE
    [SerializeField] AudioClip goButtonSE;//ゴーボタンSE
    [SerializeField] AudioClip retryButtonSE;//リトライボタンSE


    public bool playerMoving;//プレイヤー動いているか指示/条件用

    private AudioSource audioSource;//オーディオ取得用



    private void Start()
    {
        playerMoving = false;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// ゴーボタン実行にてプレイヤーのキネマティックオフ各アニメーションを消す
    /// </summary>
    public void OnClickGoButton()
    {
        Rigidbody2D rd = player.GetComponent<Rigidbody2D>();
        rd.isKinematic = false;
        retryButton.SetActive(true);
        goButton.SetActive(false);
        playerMoving = true;
        audioSource.PlayOneShot(goButtonSE);
        for (int i = 0; i <= 4; i++)
        {
            moveBaseAnimation[i].SetActive(false);
        }
        }

    /// <summary>
    /// プレイヤーを削除しプレイヤープレファブに置き換える移動を制限し各アニメーションを実行する
    /// </summary>
    public void OnClickRetryButton()
    {
        Destroy(player);
        player = Instantiate(playerPrefab);
        retryButton.SetActive(false);
        goButton.SetActive(true);
        playerMoving = false;
        audioSource.PlayOneShot(retryButtonSE);
        for (int i = 0; i <= 4; i++)
        {
            moveBaseAnimation[i].SetActive(true);
        }
    }

    /// <summary>
    /// ゴーフォーザゴールゲームシーンからステージセレクトへシーン遷移
    /// </summary>
    public void OnClickGoalBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene4",0.5f);
        audioSource.PlayOneShot(backButtonSE);
    }

    /// <summary>
    /// パルムキャッチゲームシーンからステージセレクトへシーン遷移
    /// </summary>
    public void OnClickPalmBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene6", 0.5f);
        audioSource.PlayOneShot(backButtonSE);
    }

    /// <summary>
    /// ゴールまでプレイヤーが到達したらSEとクリアテキストの表記プレイヤー移動制限/ボタンをゴーボタンとリトライボタンを消す
    /// </summary>
    public void StageClear()
    {
        audioSource.PlayOneShot(clearSE1);
        audioSource.PlayOneShot(clearSE2);
        goalAnimation.SetActive(false);
        clearText.SetActive(true);
        clearEfect.SetActive(true);
        retryButton.SetActive(false);
        backButtonAnimation.SetActive(true);
    }

}
