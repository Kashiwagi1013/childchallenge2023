using UnityEngine;

public class MoveBaseManager : MonoBehaviour
{

    [SerializeField] GameObject gameManager;//ゲームマネージャー関数呼び出し用


    /// <summary>
    /// 自信をドラッグで動かす（プレイヤーが動いてない状態にて）
    /// </summary>
    private void OnMouseDrag()
    {
        if (gameManager.GetComponent<PalmAndGoalGameManager>().playerMoving == false)
        {
            float x = Input.mousePosition.x;
            float y = Input.mousePosition.y;
            float z = 100.0f;

            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x, y, z));
        }
    }
}
