using UnityEngine;

public class PalmFruitlManager : MonoBehaviour
{


    /// <summary>
    /// 落下時にアウトエリアに接触でリトライを強制的に呼び出す
    /// </summary>
    /// <param name="coll"></param>
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "OutArea")
        {
            GameObject gameManager = GameObject.Find("PalmFruitCatchGameManager");

            gameManager.GetComponent<PalmAndGoalGameManager>().OnClickRetryButton();
        }
    }

    /// <summary>
    /// ゴールに接触でゲームクリアを呼び出す
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ClearArea")
        {
            GameObject gameManager = GameObject.Find("PalmFruitCatchGameManager");
            gameManager.GetComponent<PalmAndGoalGameManager>().StageClear();
        }
    }
}
