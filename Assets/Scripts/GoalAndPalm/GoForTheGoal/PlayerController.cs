using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    private Rigidbody2D rigid2D;//プレイヤーリジッドボディ取得用
    private float walkForce = 80.0f;//移動する際の力
    private float maxWalkSpeed = 3.0f;//移動する際の最高移動力


    private void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        PlayerSpeedPower();
    }

    /// <summary>
    /// プレイヤー落下時にアウトエリアに接触でリトライを強制的に呼び出す
    /// </summary>
    /// <param name="coll"></param>
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "OutArea")
        {
            GameObject gameManager = GameObject.Find("GoForTheGoalGameManager");

            gameManager.GetComponent<PalmAndGoalGameManager>().OnClickRetryButton();
        }
    }

    /// <summary>
    /// ゴールに接触でゲームクリアを呼び出す
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ClearArea")
        {
            GameObject gameManager = GameObject.Find("GoForTheGoalGameManager");
            gameManager.GetComponent<PalmAndGoalGameManager>().StageClear();
            walkForce = 0f;
        }
    }

    /// <summary>
    /// プレイヤー移動指示
    /// </summary>
    private void PlayerSpeedPower()
    {
        int speed = 1;

        float speedx = Mathf.Abs(this.rigid2D.velocity.x);

        if (speedx < this.maxWalkSpeed)
        {
            this.rigid2D.AddForce(transform.right * speed * this.walkForce);
        }
    }

}