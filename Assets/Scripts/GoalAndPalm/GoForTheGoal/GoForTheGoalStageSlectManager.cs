using UnityEngine;



public class GoForTheGoalStageSlectManager : MonoBehaviour
{

    [SerializeField] GameObject[] stageButtons;//ステージ選択ボタン
    [SerializeField] AudioClip ButtonSE;//ボタンSE

    private AudioSource audioSource;//オーディオ取得用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// ステージセレクトから指定ゲームシーンへ遷移
    /// </summary>
    /// <param name="stageNo"></param>
    public void OnClickStageSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("GoForTheGoalScene" + stageNo, 0.3f);
    }

    /// <summary>
    /// ステージセレクトからコースセレクトへシーン遷移
    /// </summary>
    public void OnClickBackButton()
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.3f);
    }
}
