using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ShapeMatchingStageSlectManager : MonoBehaviour
{

    public GameObject[] stageButtons;
    public AudioClip ButtonSE1;
    private AudioSource audioSource;


    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PushStageSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE1);
        FadeManager.Instance.LoadScene("ShapeMatchingScene" + stageNo, 0.3f);
    }

    public void BackButton()
    {
        audioSource.PlayOneShot(ButtonSE1);
        Invoke("GobackStageSelect", 0.2f);
    }
    void GobackStageSelect()
    {
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.3f);
    }
}
