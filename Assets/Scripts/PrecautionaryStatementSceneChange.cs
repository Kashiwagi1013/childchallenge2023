using UnityEngine;

public class PrecautionaryStatementSceneChange : MonoBehaviour
{

    private void Start()
    {
        Invoke("TitleSceneChange", 5);
    }

    private void TitleSceneChange()
    {
        FadeManager.Instance.LoadScene("TitleScene", 0.1f);
    }
}
