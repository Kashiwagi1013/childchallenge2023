using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using DG.Tweening;

public class CardCreateManager : MonoBehaviour
{

    [SerializeField] CardController cardPrefab;//生成するCardオブジェクト
    [SerializeField] RectTransform cardCreateParent;//「カード」を生成する親オブジェクト
    [SerializeField] List<CardController> cardList = new List<CardController>();//生成したカードオブジェクトを保存する
    [SerializeField] GridLayoutGroup gridLayout;//GridLayoutGroup

    private List<CardData> randomCardDataList = new List<CardData>();//カード情報の順位をランダムに変更したリスト
    private int createCardCount;//生成するカードのカウント数（ステージNo.に応じて）
    private int index;//カード配列のインデックス
    private int helgthIdx;//カードを生成する時の高さインデックス
    private int widthIdx;//カードを生成する時の幅インデックス
    private readonly float dealCaedTime = 0.2f;//カードの生成アニメーションのアニメーション時間


    private void Start()
    {

        RandumCardPlacement();

        GameContentsNumber();

        //GridLayoutを無効
        this.gridLayout.enabled = false;

        //カードを配るアニメーション処理
        this.mSetDealCardAnime();


    }
    /// <summary>
    /// ステージに合わせて配置するカードの総枚数を決める
    /// </summary>
    private void GameContentsNumber()
    {

        Debug.Log(GameSceneManager.stageNumber);
        if (GameSceneManager.stageNumber == 1)
            createCardCount = 12;
        if (GameSceneManager.stageNumber == 2)
            createCardCount = 16;
        if (GameSceneManager.stageNumber == 3)
            createCardCount = 20;
        if (GameSceneManager.stageNumber == 4)
            createCardCount = 24;
        if (GameSceneManager.stageNumber == 5)
            createCardCount = 28;

    }

    /// <summary>
    /// カードデータをリスト化し指定の数だけランダム配置する
    /// </summary>
    private void RandumCardPlacement()
    {

        //カード情報リスト
        List<CardData> cardDataList = new List<CardData>();

        //表示するカード画像情報のリスト
        List<Sprite> imgList = new List<Sprite>();

        //Resources/Imageフォルダ内にある画像を取得する
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_000"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_001"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_002"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_003"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_004"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_005"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_006"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_007"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_008"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_009"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_010"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_011"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_012"));
        imgList.Add(Resources.Load<Sprite>("Image/MemoryWeakness/card_image_013"));


        //forを回す回数を取得する
        int loopCnt = imgList.Count;

        //ステージナンバーによってカードデータのループカウントを変更
        if (GameSceneManager.stageNumber == 1)
            loopCnt = 6;
        if (GameSceneManager.stageNumber == 2)
            loopCnt = 8;
        if (GameSceneManager.stageNumber == 3)
            loopCnt = 10;
        if (GameSceneManager.stageNumber == 4)
            loopCnt = 12;
        if (GameSceneManager.stageNumber == 5)
            loopCnt = 14;
        for (int i = 0; i < loopCnt; i++)
            {

                //カード情報を生成する
                CardData cardata = new CardData(i, imgList[i]);
                cardDataList.Add(cardata);
            }
        
        this.index = 0;
        this.helgthIdx = 0;
        this.widthIdx = 0;

        //生成したカードリスト２つ分のリストを生成する
        List<CardData> SumCardDataList = new List<CardData>();
        SumCardDataList.AddRange(cardDataList);

        //リストの中身をランダムに再配置する
        this.randomCardDataList = SumCardDataList.OrderBy(a => Guid.NewGuid()).ToList();
        this.randomCardDataList.AddRange(SumCardDataList.OrderBy(a => Guid.NewGuid()).ToList());
    }

    /// <summary>
    /// カードを生成する
    /// </summary>
    public void CreateCard()
    {
        //カード情報リスト
        List<CardData> cardDataList = new List<CardData>();
        //生成したカードリスト２つ分のリストを生成する
        List<CardData> SumCardDataList = new List<CardData>();
        SumCardDataList.AddRange(cardDataList);
        //リストの中身をランダムに再配置する
        List<CardData> randomCardDataList = SumCardDataList.OrderBy(a => Guid.NewGuid()).ToList();
        //カードオブジェクトを生成する
        foreach (var _cardData in randomCardDataList)
        {
            //Instantiate で Cardオブジェクトを生成
            CardController card = Instantiate<CardController>(this.cardPrefab, this.cardCreateParent);
            //データを設定する
            card.Set(_cardData);
            //生成したカードオブジェクトを保存する
            this.cardList.Add(card);
        }
    }

    // <summary>
    /// 取得していないカードを背面にする
    /// </summary>
    public void HideCardList(List<int> containCardIdList)
    {

        foreach (var _card in this.cardList)
        {
            //既に獲得したカードIDの場合、非表示にする
            if (containCardIdList.Contains(_card.id))
            {

                //カードを非表示にする
                _card.SetInvisible();
            }
            //カードが表麺 && 獲得していないカードは裏面表示にする
            else if (_card.isSelected)
            {
                //カードを裏面表示にする
                _card.SetHide();
            }
        }
    }

    /// <summary>
    /// カードを配るアニメーション処理
    /// </summary>
    private void mSetDealCardAnime()
    {

        var _cardData = this.randomCardDataList[this.index];

        //Instantiate で Cardオブジェクトを生成
        CardController card = Instantiate<CardController>(this.cardPrefab, this.cardCreateParent);
        //データを設定する
        card.Set(_cardData);
        //カードの初期値を設定 (画面外にする)
        card.rectTransform.anchoredPosition = new Vector2(1900, 0f);
        //サイズをGridLayoutのCellSizeに設定
        card.rectTransform.sizeDelta = this.gridLayout.cellSize;

        //カードの移動先を設定
        float posX = (this.gridLayout.cellSize.x * this.widthIdx) + (this.gridLayout.spacing.x * (this.widthIdx + 1));
        float posY = ((this.gridLayout.cellSize.y * this.helgthIdx) + (this.gridLayout.spacing.y * this.helgthIdx)) * -1f;

        //DOAnchorPosでアニメーションを行う
        card.rectTransform.DOAnchorPos(new Vector2(posX, posY), this.dealCaedTime)
            //アニメーションが終了したら
            .OnComplete(() => {
                //生成したカードオブジェクトを保存する
                this.cardList.Add(card);

                //生成するカードデータリストのインデックスを更新
                this.index++;
                this.widthIdx++;

                //生成インデックスがリストの最大値を迎えたら
                if (this.index >= createCardCount)
                {
                    //GridLayoutを有効にし、生成処理を終了する
                    this.gridLayout.enabled = true;
                }
                else
                {
                    //GridLayoutの折り返し地点に来たら
                    if (this.index % this.gridLayout.constraintCount == 0)
                    {
                        // 高さの生成箇所を更新
                        this.helgthIdx++;
                        this.widthIdx = 0;
                    }
                    //アニメーション処理を再帰処理する
                    this.mSetDealCardAnime();
                }
            });
    }
}