using UnityEngine;


public class MemoryWeaknessStageSlectManager : MonoBehaviour
{

    [SerializeField] GameObject[] stageButtons;//ステージ選択ボタン
    [SerializeField] AudioClip ButtonSE;//ボタンSE

    private AudioSource audioSource;//オーディオ再生用

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// ステージセレクトから各ゲームシーンへ遷移
    /// </summary>
    /// <param name="stageNo"></param>
    public void OnClickStageSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("MemoryWeaknessScene" + stageNo, 0.3f);
    }

    /// <summary>
    /// ステージセレクトからコースセレクトへ遷移
    /// </summary>
    public void OnClickBackButton()
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.3f);
    }

}
