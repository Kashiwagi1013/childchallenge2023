using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    [SerializeField] int stageNumberEdit;//ステージナンバーインスペクター表示用
    [SerializeField] GameObject clearText;//クリア時表記テキスト
    [SerializeField] GameObject clearEfect;//ゲームクリア時エフェクト表示
    [SerializeField] GameObject backButtonAnimation;//クリア時表示用バックボタンアニメーション
    [SerializeField] AudioClip clearSE1;//クリア時SE
    [SerializeField] AudioClip clearSE2;//クリア時SE
    [SerializeField] AudioClip backButtonSE;//バックボタンSE
    [SerializeField] AudioClip succesButtonSE;//絵柄が合致する時SE
    [SerializeField] AudioClip disagreeButtonSE;//絵柄が合わないSE

    
    public static int stageNumber;//ステージナンバー
    public CardCreateManager cardCreate;//カード生成マネージャクラス


    private int cardCount;//カードのカウント数
    private AudioSource audioSource;//オーディオ再生用
    private bool isCalledOnce = false;//クリア時一度だけ呼び出し用
    private List<int> containCardIdList = new List<int>();//一致したカードリストID



    private void Awake()
    {
        //ステージナンバー代入処理
        stageNumber = stageNumberEdit;

        audioSource = gameObject.GetComponent<AudioSource>();

        //一致したカードIDリストを初期化
        this.containCardIdList.Clear();

        //カードリストを生成する
        this.cardCreate.CreateCard();

        GameContentsNumber();
    }


    private void Update()
    {
        GameContents();
    }

    /// <summary>
    /// ステージに合わせてクリアするカードの枚数を変更
    /// </summary>
    private void GameContentsNumber()
    {
        if (stageNumber == 1)
            cardCount = 6;
        if (stageNumber == 2)
            cardCount = 8;
        if (stageNumber == 3)
            cardCount = 10;
        if (stageNumber == 4)
            cardCount = 12;
        if (stageNumber == 5)
            cardCount = 14;
    }

    /// <summary>
    /// ゲームルールの適用部分（指示する役目）
    /// </summary>
    private void GameContents()
    {
        //選択したカードが２枚以上になったら
        if (GameStateController.Instance.selectedCardIdList.Count >= 2)
        {

            //最初に選択したCardIDを取得する
            int selectedId = GameStateController.Instance.selectedCardIdList[0];

            //2枚目にあったカードと一緒だったら
            if (selectedId == GameStateController.Instance.selectedCardIdList[1])
            {

                Debug.Log($"Contains! {selectedId}");
                //一致したカードIDを保存する
                this.containCardIdList.Add(selectedId);
                audioSource.PlayOneShot(succesButtonSE);
            }
            //カードが揃わなかったら絵柄が合わないSE
            else
            {
                audioSource.PlayOneShot(disagreeButtonSE);
            }

            //カードの表示切り替えを行う
            this.cardCreate.HideCardList(this.containCardIdList);

            //選択したカードリストを初期化する
            GameStateController.Instance.selectedCardIdList.Clear();

            //ゲームのクリア条件を満たしゲームクリアを呼び出す
            if (containCardIdList.Count >= cardCount)
                if (!isCalledOnce)
                {
                    isCalledOnce = true;
                    clearText.SetActive(true);
                    clearEfect.SetActive(true);
                    audioSource.PlayOneShot(clearSE1);
                    audioSource.PlayOneShot(clearSE2);
                    backButtonAnimation.SetActive(true);
                }
        }
    }

    /// <summary>
    /// ステージセレクトへ遷移する
    /// </summary>
    public void OnClickBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene8", 0.5f);
        audioSource.PlayOneShot(backButtonSE);
    }
}
