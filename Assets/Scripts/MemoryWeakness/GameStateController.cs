using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{

    public List<int> selectedCardIdList = new List<int>();//選択されたカードIDリスト


    private static GameStateController mInstance;//シングルトンの生成

    public static GameStateController Instance
    {
        get
        {
            // インスタンスが生成されていない場合、自動で生成する
            if (mInstance == null)
            {
                GameObject obj = new GameObject("GameStateController");
                mInstance = obj.AddComponent<GameStateController>();
            }
            return mInstance;
        }
    }
}
