using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ColorMatchingStageSlectManager : MonoBehaviour
{

    public GameObject[] stageButtons;
    public AudioClip ButtonSE1;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void PushStageSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE1);
        FadeManager.Instance.LoadScene("ColorMatchingScene" + stageNo, 0.3f);
    }

    public void BackButton()
    {
        audioSource.PlayOneShot(ButtonSE1);
        Invoke("GobackStageSelect", 0.2f);
    }
    void GobackStageSelect()
    {
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.3f);
    }
}
