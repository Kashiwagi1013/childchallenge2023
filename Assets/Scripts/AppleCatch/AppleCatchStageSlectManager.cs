using UnityEngine;

public class AppleCatchStageSlectManager : MonoBehaviour
{


    [SerializeField] GameObject[] stageButtons;//ステージナンバー
    [SerializeField] AudioClip ButtonSE1;//ボタン実行時SE


    private AudioSource audioSource;//オーディオ再生用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// ボタンが押されたらアップルキャッチのゲームシーンへ遷移
    /// </summary>
    /// <param name="stageNo"></param>
    public void OnClickSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE1);
        FadeManager.Instance.LoadScene("AppleCatchScene" + stageNo, 0.3f);
    }

    /// <summary>
    /// ゲームセレクト画面からコース選択画面へ遷移
    /// </summary>
    public void OnClickBackButton()
    {
        audioSource.PlayOneShot(ButtonSE1);
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.5f);
    }
}
