using UnityEngine;

public class ItemPrefabController : MonoBehaviour
{



    private void FixedUpdate()
    {
        ItemPrefabControll();
    }

    /// <summary>
    /// アイテムプレファブ自信の出現/消滅条件の設定
    /// </summary>
    private void ItemPrefabControll()
    {
        if (transform.position.y < -3.8f)
        {
            GetComponent<ParticleSystem>().Play();
        }
        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }
    }
}
