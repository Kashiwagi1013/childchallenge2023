using UnityEngine;

public class AppleCatchPlayerController : MonoBehaviour
{


    [SerializeField] AudioClip appleGetSE;//アップル所得時SE
    [SerializeField] AudioClip insectGetSE;//虫取得時SE
    [SerializeField] GameObject appleManager;//アップルマネージャー関数呼び出し用


    private AudioSource audioSource;//オーディオ再生用



    private void Start()
    {
        this.audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// アイテム取得条件の設定
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        //アップルタグの処理
        if(other.gameObject.tag == "Apple")
        {
            this.appleManager.GetComponent<AppleCatchManager>().GetApple();
            this.audioSource.PlayOneShot(this.appleGetSE);
        }

        //インセクトタグの処理
        else if (other.gameObject.tag == "Insect")
        {
            this.appleManager.GetComponent<AppleCatchManager>().GetInsect();
            this.audioSource.PlayOneShot(this.insectGetSE);
            GetComponent<ParticleSystem>().Play();
        }
        Destroy(other.gameObject);
    }

    /// <summary>
    /// プレイヤー移動処理ドラッグで操作
    /// </summary>
    private void OnMouseDrag()
    {
        //プレイヤーの位置をワールド座標からスクリーン座標に変換して、objectPointに格納
        Vector3 objectPoint = Camera.main.WorldToScreenPoint(transform.position);

        //プレイヤーの現在位置(マウス位置)を、pointScreenに格納
        Vector3 pointScreen = new Vector3(Input.mousePosition.x, objectPoint.y, objectPoint.z);

        //プレイヤーの現在位置を、スクリーン座標からワールド座標に変換して、pointWorldに格納
        Vector3 pointWorld = Camera.main.ScreenToWorldPoint(pointScreen);
        pointWorld.z = transform.position.z;

        //プレイヤーの位置を、pointWorldにする
        transform.position = pointWorld;

        //プレイヤーの位置を取得
        pointWorld = transform.position;

        //x位置が指定範囲内か確認-8.8の間
        pointWorld.x = Mathf.Clamp(pointWorld.x, -8, 8);

        //範囲内であれば常にその位置がそのまま入る
        transform.position = new Vector2(pointWorld.x, pointWorld.y); 
    }
}
