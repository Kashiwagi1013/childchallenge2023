using UnityEngine;
using UnityEngine.UI;

public class AppleCatchManager : MonoBehaviour
{
    [SerializeField] int StageNo;//ステージナンバー
    [SerializeField] int parameterRatio;//パラメーター設定用比率
    [SerializeField] float parameterSpan;//パラメーター設定用スパン
    [SerializeField] GameObject applePrefab;//アップルプレファブ
    [SerializeField] GameObject insectPrefab;//虫プレファブ
    [SerializeField] GameObject timeCountText;//タイムカウント用テキスト
    [SerializeField] GameObject pointText;//ポイント取得時用テキスト
    [SerializeField] GameObject clearText;//クリア時テキスト表示用
    [SerializeField] GameObject clearEfect;//ゲームクリア時エフェクト表示
    [SerializeField] GameObject reChallengeText;//未クリア時のテキスト表示
    [SerializeField] GameObject reStartButton;//リスタートボタン
    [SerializeField] GameObject appleCatchPlayer;//プレイヤー
    [SerializeField] GameObject backButtonAnimation;//クリア時のバックボタン指示用アニメーション
    [SerializeField] GameObject reStartButtonAnimation;//リスタートボタン用アニメーション
    [SerializeField] AudioClip clearSE1;//クリア時SE1
    [SerializeField] AudioClip clearSE2;//クリア時SE2
    [SerializeField] AudioClip backButtonSE;//バックボタンSE
    [SerializeField] AudioClip reChallengeSE;//未クリア時のSE
    [SerializeField] Text timeCountDownText;//タイムカウントダウン用テキスト


    private float countDown = 3.0f;//タイムカウントダウン用3秒後にスタート設定
    private float itemInstanceSpan;//アイテムインスタンス用スパン（秒）
    private float itemInstanceCount = 0;//アイテムインスタンス用カウント
    private float time = 30f;//制限時間を設定30秒
    private int count;//カウントダウンを1桁で表記用
    private int itemInstanceRatio;//アイテムインスタンス用出現比率（アップル/虫）
    private int point = 0;//初期設定ポイント0
    private bool isCalledOnce = false;//アップデート管理なのでクリア時に一回だけ呼び出す為に必要
    private AudioSource audioSource;//オーディオ再生用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    private void FixedUpdate()
    {
        GameSetStart();
        ItemInstance();
    }

    /// <summary>
    /// アイテムインスタンス時の設定用パラメーター
    /// </summary>
    /// <param name="span"></param>
    /// <param name="ratio"></param>
    private void ItemInstanceSetParameter(float span, int ratio)
    {
        this.itemInstanceSpan = span;
        this.itemInstanceRatio = ratio;
    }

    /// <summary>
    /// ゲーム開始からゲーム終了までの設定を指示
    /// </summary>
    private void GameSetStart()
    {
        //ゲームのカウントダウン開始3秒
        if (countDown >= 0)
        {
            countDown -= Time.deltaTime;
            count = (int)countDown;
            timeCountDownText.text = count.ToString();
        }

        //ゲームカウントダウンが0になったらゲーム開始UI表示/非表示
        if (countDown <= 0)
        {
            timeCountDownText.text = "";

            this.time -= Time.deltaTime;

            this.timeCountText.GetComponent<Text>().text =
                this.time.ToString("F1");

            this.pointText.GetComponent<Text>().text =
                this.point.ToString() + " Apple";

            if (this.time < 0)
            {
                this.time = 0;
                ItemInstanceSetParameter(10000.0f, 0);
            }
            else if (30 <= this.time && this.time < 40)
                ItemInstanceSetParameter(10000.0f, 0);
          
            else if (3 <= this.time && this.time < 30)
                ItemInstanceSetParameter(parameterSpan, parameterRatio);

            else if (0 <= this.time && this.time < 3)
                ItemInstanceSetParameter(10000.0f, 0);

            //終了までに5ポイント以上取得していたらゲームクリア画面を表示
            if (5 <= this.point && this.time == 0)
            {
                if (!isCalledOnce)
                {
                    isCalledOnce = true;
                    clearText.SetActive(true);
                    clearEfect.SetActive(true);
                    timeCountText.SetActive(false);
                    pointText.GetComponent<ParticleSystem>().Play();
                    audioSource.PlayOneShot(clearSE1);
                    audioSource.PlayOneShot(clearSE2);
                    appleCatchPlayer.SetActive(false);
                    backButtonAnimation.SetActive(true);
                }
            }

            //終了までに5ポイント取得していなかったらリスタート画面を表示
            if (5 >= this.point && this.time == 0)
            {
                if (!isCalledOnce)
                {
                    isCalledOnce = true;
                    reChallengeText.SetActive(true);
                    timeCountText.SetActive(false);
                    appleCatchPlayer.SetActive(false);
                    audioSource.PlayOneShot(reChallengeSE);
                    Invoke("ReStartButtonActive", 1);
                    backButtonAnimation.SetActive(true);
                    reStartButtonAnimation.SetActive(true);
                }
            }
        }
    }

    /// <summary>
    /// アイテムインスタンス出現設定/指示
    /// </summary>
    private void ItemInstance()
    {

        if (countDown <= 0)
        {
            this.itemInstanceCount += Time.deltaTime;
            if (this.itemInstanceCount > this.itemInstanceSpan)
            {
                this.itemInstanceCount = 0;
                GameObject item;
                int dice = Random.Range(1, 11);
                if (dice <= this.itemInstanceRatio)
                {
                    item = Instantiate(insectPrefab) as GameObject;
                }
                else
                {
                    item = Instantiate(applePrefab) as GameObject;
                }
                float x = Random.Range(8, -8);
                item.transform.position = new Vector3(x, 5, 0);
            }
        }
    }

    /// <summary>
    /// アップル取得時に1ポイント追加
    /// </summary>
    public void GetApple()
    {
        this.point += 1;
    }

    /// <summary>
    /// 虫取得時現在ポイントを半減
    /// </summary>
    public void GetInsect()
    {
        this.point /= 2;
    }

    /// <summary>
    /// 現在のゲームシーンからステージセレクトへ
    /// </summary>
    public void OnClickBackButton()
    {
        audioSource.PlayOneShot(backButtonSE);
        FadeManager.Instance.LoadScene("StageSelectScene2", 0.3f);
    }

    /// <summary>
    /// リスタートボタン押されたら現在のゲームシーンを再読み込み
    /// </summary>
    /// <param name="stageNo"></param>
    public void OnClickReStartButton(int stageNo)
    {
        FadeManager.Instance.LoadScene("AppleCatchScene" + stageNo, 0.3f);
    }

    /// <summary>
    /// 未クリア時のリスタートボタンアクティブ
    /// </summary>
    private void ReStartButtonActive()
    {
        reStartButton.SetActive(true);
    }

  
}