using UnityEngine;


public class CourseSelectManager : MonoBehaviour
{

    [SerializeField] GameObject[] stageButtons;//ステージ選択ボタン
    [SerializeField] AudioClip ButtonSE;//ボタンSE

    private AudioSource audioSource;//オーディオ設定用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// ボタンが押されたら指定のステージへ遷移する
    /// </summary>
    /// <param name="stageNo"></param>
    public void OnClickStageSelectButton(int stageNo)
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("StageSelectScene" + stageNo, 0.5f);
    }

    /// <summary>
    /// ボタンが押されたらタイトル画面へ戻る
    /// </summary>
    public void OnClickBackButton()
    {
        audioSource.PlayOneShot(ButtonSE);
        FadeManager.Instance.LoadScene("TitleScene", 0.5f);
    }
}