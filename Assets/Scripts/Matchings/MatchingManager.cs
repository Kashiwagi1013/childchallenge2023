using UnityEngine;

public class MatchingManager : MonoBehaviour
{
    [SerializeField] GameObject clearText;//ゲームクリア時テキスト表示
    [SerializeField] GameObject clearEfect;//ゲームクリア時エフェクト表示
    [SerializeField] GameObject succesText1;//絵柄1ロック時の正解○表示
    [SerializeField] GameObject succesText2;//絵柄2ロック時の正解○表示
    [SerializeField] GameObject succesText3;//絵柄3ロック時の正解○表示
    [SerializeField] GameObject succesText4;//絵柄4ロック時の正解○表示
    [SerializeField] GameObject shapeSuccesAnimation1;//絵柄1ロック時の正解アニメーション
    [SerializeField] GameObject shapeSuccesAnimation2;//絵柄2ロック時の正解アニメーション
    [SerializeField] GameObject shapeSuccesAnimation3;//絵柄3ロック時の正解アニメーション
    [SerializeField] GameObject shapeSuccesAnimation4;//絵柄4ロック時の正解アニメーション
    [SerializeField] GameObject placeSuccesAnimation1;//絵柄1ロック時の点灯アニメーションの消去
    [SerializeField] GameObject placeSuccesAnimation2;//絵柄2ロック時の点灯アニメーションの消去
    [SerializeField] GameObject placeSuccesAnimation3;//絵柄3ロック時の点灯アニメーションの消去
    [SerializeField] GameObject placeSuccesAnimation4;//絵柄4ロック時の点灯アニメーションの消去
    [SerializeField] GameObject backButtonAnimation;//クリア時バックボタンアニメーション点灯
    [SerializeField] AudioSource Sound1;//サウンドマッチング用音1
    [SerializeField] AudioSource Sound2;//サウンドマッチング用音2
    [SerializeField] AudioSource Sound3;//サウンドマッチング用音3
    [SerializeField] AudioSource Sound4;//サウンドマッチング用音4
    [SerializeField] AudioClip clearSE1;//クリア時SE1
    [SerializeField] AudioClip clearSE2;//クリア時SE2
    [SerializeField] AudioClip SuccessSE;//絵柄正解時SE
    [SerializeField] AudioClip buttonSE1;//ボタン用SE


    private AudioSource audioSource;//オーディオ取得用
    private bool isCalledOnce = false;//絵柄全てロック時の1度だけ呼び出し用
    private bool succsessShapeSE1 = false;//ロックした時に指示を呼び出す時用その1
    private bool succsessShapeSE2 = false;//ロックした時に指示を呼び出す時用その2
    private bool succsessShapeSE3 = false;//ロックした時に指示を呼び出す時用その3
    private bool succsessShapeSE4 = false;//ロックした時に指示を呼び出す時用その4



    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        SuccessShape1();
        SuccessShape2();
        SuccessShape3();
        SuccessShape4();
        AllSuccesGameClear();
    }

    /// <summary>
    /// シェイプマッチング用バックボタン
    /// </summary>
    public void OnClickShapeBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene1",0.5f);
        audioSource.PlayOneShot(buttonSE1);
    }

    /// <summary>
    /// サウンドマッチング用バックボタン
    /// </summary>
    public void OnClickSoundBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene5",0.5f);
        audioSource.PlayOneShot(buttonSE1);
    }

    /// <summary>
    /// カラーマッチング用バックボタン
    /// </summary>
    public void OnClickColorBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene3",0.5f);
        audioSource.PlayOneShot(buttonSE1);
    }

    /// <summary>
    /// シングスマッチング用バックボタン
    /// </summary>
    public void OnClickThingsBackButton()
    {
        FadeManager.Instance.LoadScene("StageSelectScene7",0.5f);
        audioSource.PlayOneShot(buttonSE1);
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその1
    /// </summary>
    private void SuccessShape1()
    {
        if (ShapeController1.locked)
            if (!succsessShapeSE1)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE1 = true;
                succesText1.SetActive(true);
                shapeSuccesAnimation1.SetActive(false);
                placeSuccesAnimation1.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその2
    /// </summary>
    private void SuccessShape2()
    {
        if (ShapeController2.locked)
            if (!succsessShapeSE2)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE2 = true;
                succesText2.SetActive(true);
                shapeSuccesAnimation2.SetActive(false);
                placeSuccesAnimation2.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその3
    /// </summary>
    private void SuccessShape3()
    {
        if (ShapeController3.locked)
            if (!succsessShapeSE3)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE3 = true;
                succesText3.SetActive(true);
                shapeSuccesAnimation3.SetActive(false);
                placeSuccesAnimation3.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその4
    /// </summary>
    private void SuccessShape4()
    {
        if (ShapeController4.locked)
            if (!succsessShapeSE4)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE4 = true;
                succesText4.SetActive(true);
                shapeSuccesAnimation4.SetActive(false);
                placeSuccesAnimation4.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が全て揃いロックし終わったらクリア指示をする
    /// </summary>
    private void AllSuccesGameClear()
    {
        if (ShapeController1.locked && ShapeController2.locked && ShapeController3.locked && ShapeController4.locked)
            if (!isCalledOnce)
            {
                isCalledOnce = true;
                clearText.SetActive(true);
                clearEfect.SetActive(true);
                backButtonAnimation.SetActive(true);
                audioSource.PlayOneShot(clearSE1);
                audioSource.PlayOneShot(clearSE2);
            }
    }

    /// <summary>
    /// サウンドマッチング用SE1/SE1意外を消音する
    /// </summary>
    public void OnClickSoundButton1()
    {
        Sound2.Stop();
        Sound3.Stop();
        Sound4.Stop();
        Sound1.Play();
    }

    /// <summary>
    /// サウンドマッチング用SE2/SE2意外を消音する
    /// </summary>
    public void OnClickSoundButton2()
    {
        Sound1.Stop();
        Sound3.Stop();
        Sound4.Stop();
        Sound2.Play();
    }

    /// <summary>
    /// サウンドマッチング用SE3/SE3意外を消音する
    /// </summary>
    public void OnClickSoundButton3()
    {
        Sound1.Stop();
        Sound2.Stop();
        Sound4.Stop();
        Sound3.Play();
    }

    /// <summary>
    /// サウンドマッチング用SE4/SE4意外を消音する
    /// </summary>
    public void OnClickSoundButton4()
    {
        Sound1.Stop();
        Sound2.Stop();
        Sound3.Stop();
        Sound4.Play();
    }


}
