using UnityEngine;

public class ShapeController3 : MonoBehaviour
{
    
    [SerializeField] Transform Place3;//絵柄が重なるマスト
    public static bool locked;//絵柄ロック伝達用


    private bool isCalledOnce = false;//絵柄ロックする用
    private bool paticleSucces = false;//正解パーティクル一度呼び出し用

    private void Start()
    {
        locked = false;
    }

    private void Update()
    {
        ShapeSuccesClear();
    }

    /// <summary>
    /// 絵柄と絵柄マストをロック/ロック指示を出す
    /// </summary>
    private void ShapeSuccesClear()
    {
        if (Mathf.Abs(transform.position.x - Place3.position.x) <= 0.8f &&
            Mathf.Abs(transform.position.y - Place3.position.y) <= 0.8f)
        {
            transform.position = new Vector3(Place3.position.x, Place3.position.y);
            locked = true;
            isCalledOnce = true;
            ShapeParticle();
        }
    }

    /// <summary>
    /// 絵柄パーティクルを一度だけ呼び出す用
    /// </summary>
    private void ShapeParticle()
    {
        if (!paticleSucces)
        {
            GetComponent<ParticleSystem>().Play();
            paticleSucces = true;
        }
    }

    /// <summary>
    /// 絵柄を移動させる指タップでドラッグ絵柄マスト付近にてロックする
    /// </summary>
    private void OnMouseDrag()
    {

        if (!isCalledOnce)
        {
            //Cubeの位置をワールド座標からスクリーン座標に変換して、objectPointに格納
            Vector3 objectPoint = Camera.main.WorldToScreenPoint(transform.position);

            //Cubeの現在位置(マウス位置)を、pointScreenに格納
            Vector3 pointScreen
                = new Vector3(Input.mousePosition.x,
                              Input.mousePosition.y,
                              objectPoint.z);

            //Cubeの現在位置を、スクリーン座標からワールド座標に変換して、pointWorldに格納
            Vector3 pointWorld = Camera.main.ScreenToWorldPoint(pointScreen);
            pointWorld.z = transform.position.z;

            //Cubeの位置を、pointWorldにする
            transform.position = pointWorld;
            //プレイヤーの位置を取得
            pointWorld = transform.position;

            //x位置が常に範囲内に入るよう指定
            pointWorld.x = Mathf.Clamp(pointWorld.x, -10, 10);

            //y位置が常に範囲内に入るよう指定
            pointWorld.y = Mathf.Clamp(pointWorld.y, -4, 3.3f);

            //範囲内であれば常にその位置がそのまま入る
            transform.position = new Vector3(pointWorld.x, pointWorld.y);
        }

    }
}
