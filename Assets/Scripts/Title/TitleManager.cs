using UnityEngine;

public class TitleManager : MonoBehaviour
{
    [SerializeField] GameObject creditView;//クレジットビュー
    [SerializeField] GameObject exitConfirmationView;//ゲーム終了確認ビュー
    [SerializeField] GameObject exitView;//ゲーム終了ビュー
    [SerializeField] AudioClip buttonSE;//スタートボタンSE

    private AudioSource audioSource;//オーディオ設定用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    /// <summary>
    /// スタートボタンが押されたらコースセレクトへシーン遷移
    /// </summary>
    public void OnClickStartButton()
    {
        audioSource.PlayOneShot(buttonSE);
        FadeManager.Instance.LoadScene("CourseSelectScene", 0.5f);
    }

    /// <summary>
    ///クレジットビュー表示（ボタン割り当て）
    /// </summary>
    public void OnClickCreditViewButton()
    {
        audioSource.PlayOneShot(buttonSE);
        creditView.SetActive(true);
    }

    /// <summary>
    ///クレジットビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickCreditViewBackButton()
    {
        audioSource.PlayOneShot(buttonSE);
        creditView.SetActive(false);
    }

    /// <summary>
    /// ゲーム終了ビュー表示（ボタン割り当て）
    /// </summary>
    public void OnClickExitViewButton()
    {
        audioSource.PlayOneShot(buttonSE);
        exitConfirmationView.SetActive(true);
    }

    /// <summary>
    /// ゲーム終了するボタン（ボタン割り当て）
    /// </summary>
    public void OnClickExitButton()
    {
        Application.Quit();
        audioSource.PlayOneShot(buttonSE);

    }

    /// <summary>
    /// ゲーム終了確認ビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickonfirmationViewBackButton()
    {
        audioSource.PlayOneShot(buttonSE);
        exitConfirmationView.SetActive(false);
    }
}
