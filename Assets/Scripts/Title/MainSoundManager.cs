using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSoundManager : MonoBehaviour
{

    public AudioSource bgm_Title;//タイトルBGM
    public AudioSource bgm_PalmFtuitCatch;//パルムキャッチBGM
    public AudioSource bgm_AppleCatch;//アップルキャッチBGM
    public AudioSource bgm_ShapeMatching;//シェイプマッチングBGM
    public AudioSource bgm_SoundMatching;//サウンドマッチングBGM
    public AudioSource bgm_ColorMatching;//カラーマッチングBGM
    public AudioSource bgm_ThingsMatching;//シングスマッチングBGM
    public AudioSource bgm_GoForTheGoal;//ゴーフォーザゴールBGM
    public AudioSource bgm_MemoryWeakness;//メモリーウィークネスBGM

    private string bgm_StageSlect = "";//ステージ現在/次遷移用


    public void Awake()
    {
        SoundNumberSet();
    }


    private void Start()
    {
        //シーンが切り替わった時に呼ばれるメソッドを登録
        SceneManager.activeSceneChanged += OnActiveSceneChanged;

    }

    /// <summary>
    /// 各シーン遷移時のサウンド指定
    /// </summary>
    private void SoundNumberSet()
    {
        int numMusicPlayers = FindObjectsOfType<MainSoundManager>().Length;
        if (numMusicPlayers > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    //シーンが切り替わった時に呼ばれるメソッド
    private void OnActiveSceneChanged(Scene prevScene, Scene nextScene)
    {
        //シーンがどう変わったかで判定

        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene1" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_ShapeMatching.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene2" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_AppleCatch.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene3" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_ColorMatching.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene4" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_GoForTheGoal.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene5" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_SoundMatching.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene6" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_PalmFtuitCatch.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene7" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_ThingsMatching.Play();
        }
        //ステージセレクトからゲームへ
        if (bgm_StageSlect == "StageSelectScene8" && nextScene.name != "CourseSelectScene")
        {
            bgm_Title.Stop();
            bgm_MemoryWeakness.Play();
        }


        //ゲームからテージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene1")
        {
            bgm_ShapeMatching.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene2")
        {
            bgm_AppleCatch.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene3")
        {
            bgm_ColorMatching.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene4")
        {
            bgm_GoForTheGoal.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene5")
        {
            bgm_SoundMatching.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene6")
        {
            bgm_PalmFtuitCatch.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene7")
        {
            bgm_ThingsMatching.Stop();
            bgm_Title.Play();
        }
        //ゲームからステージセレクトへ
        if (bgm_StageSlect != "CourseSelectScene" && nextScene.name == "StageSelectScene8")
        {
            bgm_MemoryWeakness.Stop();
            bgm_Title.Play();
        }
        //遷移後のシーン名を「１つ前のシーン名」として保持
        bgm_StageSlect = nextScene.name;
    }
}
