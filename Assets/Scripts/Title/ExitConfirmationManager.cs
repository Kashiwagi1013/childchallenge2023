using UnityEngine;

public class ExitConfirmationManager : MonoBehaviour
{
    [SerializeField] GameObject shapeSuccesAnimation1;//絵柄1ロック時の点灯アニメーションの消去
    [SerializeField] GameObject shapeSuccesAnimation2;//絵柄2ロック時の点灯アニメーションの消去
    [SerializeField] GameObject shapeSuccesAnimation3;//絵柄3ロック時の点灯アニメーションの消去
    [SerializeField] GameObject exitView;//ExitView正解時表示する用
    [SerializeField] GameObject exitConfirmationView;//ゲーム終了確認画面
    [SerializeField] AudioClip SuccessSE;//絵柄正解時SE


    private AudioSource audioSource;//オーディオ取得用
    private bool isCalledOnce = false;//絵柄全てロック時の1度だけ呼び出し用
    private bool succsessShapeSE1 = false;//ロックした時に指示を呼び出す時用その1
    private bool succsessShapeSE2 = false;//ロックした時に指示を呼び出す時用その2
    private bool succsessShapeSE3 = false;//ロックした時に指示を呼び出す時用その3


    private void Start()
    {
        ShapeController1.locked = false;
        ShapeController2.locked = false;
        ShapeController3.locked = false;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        SuccessShape1();
        SuccessShape2();
        SuccessShape3();
        AllSuccesExitView();
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその1
    /// </summary>
    private void SuccessShape1()
    {
        if (ShapeController1.locked)
            if (!succsessShapeSE1)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE1 = true;
                shapeSuccesAnimation1.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその2
    /// </summary>
    private void SuccessShape2()
    {
        if (ShapeController2.locked)
            if (!succsessShapeSE2)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE2 = true;
                shapeSuccesAnimation2.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が正解であれば絵柄をロックしアニメーションの指示をするその3
    /// </summary>
    private void SuccessShape3()
    {
        if (ShapeController3.locked)
            if (!succsessShapeSE3)
            {
                audioSource.PlayOneShot(SuccessSE);
                succsessShapeSE3 = true;
                shapeSuccesAnimation3.SetActive(false);
            }
    }

    /// <summary>
    /// 絵柄が全て揃いロックし終わったらゲーム終了画面を指示をする
    /// </summary>
    private void AllSuccesExitView()
    {
        if (ShapeController1.locked && ShapeController2.locked && ShapeController3.locked)
            if (!isCalledOnce)
            {
                isCalledOnce = true;
                exitView.SetActive(true);
                exitConfirmationView.SetActive(false);
            }
    }
}
